#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Communication avec Internet
- vérifie la possibilité d'envoyer requête GET
- vérifie configuration matérielle lié à Internet (Wifi/carte Ethernet)

Created on Tue Jan 13 17:51:55 2015

@author: Alban Thomas
"""
import os
import socket
import struct
import fcntl
from datetime import datetime
import time
import email.utils as eut

from rpi_meteo.http_handler import HTTP_Handler
import logging

logger = logging.getLogger("rpi_log."+__name__)

SIOCGIFADDR = 0x8915

class Internet(object):
    """
    Verifie la communication avec internet et essaye de la rétablir
    """
    def __init__(self, connexion, url, proxy, ssid=''):
        self._config = {'mode':connexion}
        if self._config['mode'] == 'filaire':
            self._config['iface'] = 'eth0'
        elif self._config['mode'] == 'wifi':
            self._config['iface'] = 'wlan0'
        else:
            str_msg = '%s is not an usable connection mode (wifi/filaire)' \
                      % str(connexion)
            logger.critical(str_msg)
            raise ValueError(str_msg)

        self._config['ssid'] = ssid
        self._config['ip'] = None

        self._http = HTTP_Handler(proxy)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sockfd = sock.fileno()

        # liste de sites à tester pour vérifier connection web
        self._lst_sites = ['http://www.google.fr', 'http://www.youtube.fr']
        self._lst_sites.insert(0, url)

        # vérification de la connexion avec internet
        self.connected = False
        self.date = None
        self.check()

    def _test_urls(self):
        """
        Teste communication avec serveur + 2 autres sites censes etre OK
        """
        self.connected = False
        for url in self._lst_sites:
            if self._http.check_url(url):
                self._set_date(self._http.get_request(url, None))
                logger.debug('Head request OK : %s', url)
                self._get_ip()
                self.connected = True
                break
            else:
                logger.debug('Head request fails : %s', url)
        return self.connected

    def _see_ssid(self):
        """
        Test si le réseau wifi est connecté
        """
        rep = os.system('iwlist wlan0 scanning | grep %s'
                        % self._config['ssid'])
        if rep == 0:
            logger.debug('SSID %s available', self._config['ssid'])
            return True
        else:
            logger.warning('SSID %s not available', self._config['ssid'])
            return False

    def check(self):
        """
        Procédure général de test de connexion à internet
        1. Envoi 1 requête WEB à qq sites web
        (2. Vérification DNS => ping sur IP)
        (3. Vérification adressage IP ?)
        4. Vérification configuration Wifi/Eth et réactiver connexion si
        nécessaire
        """
        if self._test_urls():
            logger.info('Internet connected => %s - %s', self._config['mode'],
                        self._config['ip'])
            return True

        self._dhcp()  # vérifie attribution IP
        if self._test_urls():
            logger.info('Internet reconnected')
            return True

        for i in range(3):
            logger.info('Internet connection => attempt %i/3...', i + 1)
            logger.debug('%s desactivated for %i sec...',
                         self._config['iface'], i)
            os.system('ifdown %s' % self._config['iface'])
            time.sleep(i)
            logger.debug('Restart %s (%i/3)...', self._config['iface'], i + 1)
            os.system('ifup --force %s' % self._config['iface'])
            time.sleep(10)
            if self._config['mode'] == "wifi":
                self._see_ssid()  # verifie que le ssid est bien dispo
                # logger.info('Wifi configuring...')
                # os.system('iwconfig wlan0 essid %s' % self._config['ssid'])
            self._dhcp()  # vérifie attribution IP
            if self._test_urls():
                logger.info('Internet reconnected (%i/3) => %s - %s', i + 1,
                            self._config['mode'], self._config['ip'])
                return True
        logger.warning('Unable to reconnect to internet')
        return False

    def request(self, url, fields):
        """
        Envoie une requete GET à une URL donnée, et des paramètres spécifiés
        La date de la connexion est enregistrée
        """
        if not self.connected:
            self.check()
        str_date = self._http.get_request(url, fields)
        self._set_date(str_date)
        return str_date

    def _set_date(self, date):
        """
        Enregistre la date d'une connexion à un site Web, à partir d'une date
        sous forme de chaine de caracteres
        """
        if date is None:
            self.date = None
            self.connected = False
            logger.warning('Date missing : no web connection')
        else:
            date = eut.parsedate(date)
            self.date = datetime(*date[:6])
            logger.debug('Date derniere connexion : %s',
                         self.date.strftime('%Y-%m-%d %H:%M:%S'))

    def _dhcp(self, release=False):
        """
        Get or Release an IP adress from DHCP server
        """
        self._get_ip()
        if self._config['ip'] is None:
            logger.debug('Trying to get IP adress for %s',
                         self._config['iface'])
            if release:
                logger.debug('Release DHCP bail for %s', self._config['iface'])
                os.system('dhclient -r %s' % self._config['iface'])
            else:
                logger.debug('Trying to get IP adress for %s',
                             self._config['iface'])
                os.system('dhclient %s' % self._config['iface'])
            logger.debug('IP address asked')

    def _get_ip(self):
        """
        Obtenir adresse IP
        """
        self._config['ip'] = None
        ifreq = struct.pack('16sH14s', self._config['iface'],
                            socket.AF_INET, '\x00'*14)
        try:
            res = fcntl.ioctl(self._sockfd, SIOCGIFADDR, ifreq)
        except:
            logger.warning('No available IP address')
            return None
        ip_adress = struct.unpack('16sH2x4s8x', res)[2]
        self._config['ip'] = socket.inet_ntoa(ip_adress)
        logger.debug('Get IP address : %s', self._config['ip'])
        return self._config['ip']
