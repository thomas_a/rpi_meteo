#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Ordinateur servant d'intermediaire entre la station et internet
Created on Tue Feb 10 17:06:31 2015

@author: thomas_a
"""
import os
from datetime import datetime, timedelta
import logging

logger = logging.getLogger("rpi_log."+__name__)


class Computer(object):
    """
    Interaction avec l'ordinateur : lecture et définition du temps
    """
    _DATE_FMT = "%Y-%m-%d %H:%M:%S"
    __NTP_FILE = '/var/lib/ntp/ntp.drift'
    def __init__(self):
        self.__date = None
        self.connected = False

    @property
    def date(self):
        """
        Renvoie l'heure de l'ordinateur au format datetime
        """
        self.connected = False
        date_ntp = datetime(1970, 1, 1)
        self.__date = datetime.utcnow()
        try:
            date_ntp = datetime.utcfromtimestamp(os.path.getmtime( \
                                                 self.__NTP_FILE))
            logger.debug('NTP : last connection on %s',
                         date_ntp.strftime(self._DATE_FMT))
        except (IOError, OSError):
            logger.warning('Issue with NTP drift file')

        # si connection récente au serveur NTP => OK
        if abs(date_ntp - self.__date) < timedelta(hours=3):
            logger.info('PC time = %s (trustable)',
                        self.__date.strftime(self._DATE_FMT))
            self.connected = True
        else:
            logger.info('PC time = %s (NOT trustable)',
                        self.__date.strftime(self._DATE_FMT))

        return self.__date

    @property
    def str_date(self):
        """
        Renvoie l'heure en string (format américain)
        """
        self.__date = datetime.utcnow()
        sdate = datetime.strftime(self.__date, self._DATE_FMT)
        return sdate

    @date.setter
    def date(self, date):
        """
        Definit la date de l'ordinateur
        """
        if type(date) != datetime:
            logger.error('Invalid date : cannot set raspberry date')
            raise TypeError
        else:
            str_date = date.strftime('%d %b %Y %H:%M:%S')

        rep = os.system('date -s "%s" --utc' % str_date)
        logger.debug('date -s "%s" --utc', str_date)

        if rep == 0:
            self.__date = date
            logger.info('Raspberry date set to %s',
                        date.strftime(self._DATE_FMT))
            return True
        else:
            logger.warning('Raspberry date can not be set to %s',
                           date.strftime(self._DATE_FMT))
            return False
