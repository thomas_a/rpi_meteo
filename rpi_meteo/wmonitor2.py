#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Communication avec station Davis Weather Monitor II
Created on Fri Mar  7 12:28:13 2014

@author: thomas_a
"""
import serial
import struct
from datetime import date
import time
import logging
import termios

from rpi_meteo.base import Base
from rpi_meteo.crc import CRC

logger = logging.getLogger("rpi_log."+__name__)

wmIIcrc = CRC()


class WeatherMII(object):
    """
    Communication avec Station DAVIS Weather Monitor II
    """
    __baud = None
    __DELAY = 2
    _RETRY = 5
    fields = dict()  # donnees météo
    # device reply commands
    _ACK = 0x06
    _NOK = 0x21

    def __init__(self, device, log_interval):
        for self.__baud in [1200, 2400]:
            try:
                self.port = serial.Serial(device, self.__baud,
                                          timeout=self.__DELAY,
                                          stopbits=serial.STOPBITS_ONE,
                                          bytesize=serial.EIGHTBITS,
                                          rtscts=True, dsrdtr=False)
            except serial.SerialException:
                self.port = None
                logger.warning('Connexion impossible sur %s (%i baud)',
                               device, self.__baud)
                continue
        if self.port is None:
            logger.error('Permission refusee sur %s', device)
            return None

        self.__clear_receive_buffer()
        self._base = Base()

        self.get_model()

        self._set_archive_interval(log_interval)
        self.__cal = dict()  # parametres de calibration
        """
        # TODO : récupérer les valeurs de calibration...
        self.__get_cal()
        rep=0
        while rep==0:
            rep=self._get_archive_fields()
        """
        logger.info('Console Weather Monitor II prete')

    def __del__(self):
        """
        Ferme le port série quand l'objet WeatherMII est fermé
        """
        try:
            self.port.close()
        except AttributeError:
            logger.error('Fermeture station => port inexistant')
        logger.info('Station meteo fermée')

    def _check_ack(self):
        """
        Check Acknowledgement
        """
        resp = self.port.read()
        if resp == '':
            logger.debug("Timeout waiting for ack")
            return False

        ack = struct.unpack("<B", resp)[0]
        if ack == self._ACK:
            logger.debug("ACK => Response OK!")
        elif ack == self._NOK:
            logger.warning("ACK => Response NOK!")
            return False
        else:
            logger.error("ACK => not ACK, not NOK either... (%s)", ack)
            return False
        return True

    def _cmd(self, cmd, *args):
        '''
        write a single command, with variable number of arguments. after the
        command, the device must return ACK
        '''
        log_cmd = cmd + ' '
        if args:
            cmd = "%s%s" % (cmd, ''.join(str(a) for a in args))
            log_cmd += ' '.join(str(a).encode('hex') for a in args)

        cmd += '\r'
        log_cmd = log_cmd.strip()

        for i in xrange(self._RETRY):
            time.sleep(i/float(self._RETRY))
            self.__clear_receive_buffer()
            logger.debug("SEND : %s (%i/%i)...", log_cmd, i + 1, self._RETRY)
            try:
                self.port.write(cmd)
            except serial.writeTimeoutError:
                logger.debug("SEND : %s (%i/%i) => TIMEOUT",
                             log_cmd, i + 1, self._RETRY)
                continue
            except termios.error:
                logger.debug("SEND : %s (%i/%i) => TTY error",
                             log_cmd, i + 1, self._RETRY)
                continue
            if self._check_ack():
                logger.debug("SEND (%i/%i) : %s => OK", i+1,
                             self._RETRY, log_cmd)
                return True
            else:
                logger.warning("SEND (%i/%i) : %s => No ACK", i+1,
                               self._RETRY, log_cmd)
                continue
        logger.warn('Can not access weather station')
        return False

    def get_model(self):
        """
        Renvoie le modele de la station ("readStationMake" dans code Davis)
        """
        #self.__clear_receive_buffer()
        buf = self.__read_link(1, 0, 0x4D)
        model = struct.unpack("<B", buf)[0]
        dct_model = {0: 'Wizard III', 1: 'Wizard II', 2: 'Monitor',
                     3: 'Perception', 4: 'GroWeather', 5: 'Energy',
                     6: 'Health'}
        if model in dct_model:
            logger.info('Modele de station : %s', dct_model[model])
            return dct_model[model]
        else:
            logger.error('Modele de station inconnu (%i)', model)
            return None

    def _get_archive_fields(self):
        """
        Recupere les donnees archivees

        TODO: autre solution avec commande DMP
        # commande la lecture de l'archive
        if self._cmd("DMP"):
            logger.info('DMP reussi')
            return True
        else:
            logger.warn('DMP echoue')
            return False
        """
        lst_flds = ['arcDateStation', 'LastArchiveTime',
                    'avgTempOut', 'hiTempOut', 'loTempOut', 'avgTempIn',
                    'arcPressure', 'arcHumIn', 'arcHumOut', 'arcGust',
                    'WDirs', 'avgWindSpeed', 'domWindDir',
                    'PrevRain', 'arcRainFall', 'SamplePer', 'ArcPeriod']

        self.fields['SamplePer'] = self._get_sample_period()
        self.fields['ArcPeriod'] = self._get_archive_interval()
        buf = self.__read_link(4, 1, 0x40)
        r = struct.unpack("<H", buf)
        self.fields['PrevRain'] = (r[0]/100.)  # h  => 2bytes
        # => 4 bytes / 8  nibbles

        buf = self.__read_link(4, 1, 0x48)
        r = struct.unpack("<H", buf)
        self.fields['LastArchiveTime'] = r[0]  # H => 2bytes

        buf = self.__read_link(32, 1, 0x5E)
        r = struct.unpack("<8H", buf)
        self.fields['WDirs'] = r  # 8H =>  => 32bytes

        buf = self.__read_link(42, 1, 0x88)
        r = struct.unpack("<HBBHhhBBhB4Bh", buf)

        self.fields['arcPressure'] = r[0]/1000.
        self.fields['arcHumIn'] = r[1]
        self.fields['arcHumOut'] = r[2]
        self.fields['arcRainFall'] = r[3]
        self.fields['avgTempIn'] = r[4]/10.
        self.fields['avgTempOut'] = r[5]/10.
        self.fields['avgWindSpeed'] = r[6]
        self.fields['domWindDir'] = r[7]
        self.fields['hiTempOut'] = r[8]/10.
        self.fields['arcGust'] = r[9]
        ts = [self._base.encode(i) for i in r[10:13]]
        self.fields['arcDateStation'] = '%02i-%02i-%i - %02i:%02i' \
            % (date.today().year, ts[2], r[13] % 16, ts[0], ts[1])
        self.fields['loTempOut'] = r[14]/10.
        msg = [str(self.fields[i]) for i in lst_flds]
        logger.debug("Data ARCHIVE: "+";".join(msg))

    def _get_sensor_fields(self):
        """ Lecture des donnees RTC """
        buf = self.__read_link(4, 1, 0x00)
        r = struct.unpack("<H", buf)
        self.fields['NextRec'] = r[0]

        nb_loops = struct.pack("<H", 0xFFFF)
        lst_flds = ['TempIn', 'TempOut', 'WindSpeed', 'WindDir', 'Pressure',
                    'HumIn', 'HumOut', 'RainRate']

        if not self._cmd("LOOP", nb_loops):
            logger.error("Commande LOOP echouee")
            return None
        buf = self.port.read(size=18)
        response = struct.unpack("<BhhBHHBBHHH", buf)

        if response[0] != 0x01:
            raise Exception("Start of block-byte was not 0x01")

        if not wmIIcrc.verify(buf[1:]):
            raise Exception("bad CRC Checking")

        self.fields['TempIn'] = response[1]/10.
        self.fields['TempOut'] = response[2]/10.
        self.fields['WindSpeed'] = response[3]
        self.fields['WindDir'] = response[4]
        self.fields['Pressure'] = response[5]/1000.
        self.fields['HumIn'] = response[6]
        self.fields['HumOut'] = response[7]
        self.fields['RainRate'] = response[8]/100.
        msg = [str(self.fields[i]) for i in lst_flds]
        logger.debug("Data CAPTEUR: "+";".join(msg))

    def __get_cal(self):
        """
        Lecture des nombres nécessaires à calibration
        """
        # temperature interieure
        if self._cmd("WRD", "\x44", "\x52"):
            buf = self.port.read(2)
            self.__cal['inTemp'] = struct.unpack('<H', buf)[0]

        # temperature exterieure
        if self._cmd('WRD', "\x34", "\x45"):
            buf = self.port.read(2)
            tmp_out_cal = struct.unpack('<H', buf)[0]
            if tmp_out_cal > 2048:
                tmp_out_cal -= 4096
            self.__cal['outTemp'] = tmp_out_cal

        # humidite
        if self._cmd('WRD', "\x44", "\xDA"):
            buf = self.port.read(2)
            self.__cal['outHum'] = struct.unpack('<H', buf)[0]

        # pression
        if self._cmd('WRD', "\x44", "\x2C"):
            buf = self.port.read(2)
            self.__cal['Pressure'] = struct.unpack('<H', buf)[0]

        # precipitations
        if self._cmd('WRD', "\x44", "\xD6"):
            buf = self.port.read(2)
            self.__cal['Rain'] = struct.unpack('<H', buf)[0]

    def _set_archive_interval(self, minutes):
        """
        Definit l'interval d'enregistrement et la periode d'enregistrement
        correspondante
        """
        dct_archive = {1: 5, 5: 5, 10: 5, 15: 8, 30: 15, 60: 30, 120: 60}

        # Verifie si periode d'archivage est bien definie
        if self._get_archive_interval() == minutes:
            logger.info("Intervalle d'archivage OK (%i min.)", minutes)
            return 0
        else:
            logger.info("Archive interval : %i => %imin.",
                        self._get_archive_interval(), minutes)

        # definition intervalle archivage + periode echantillonnage
        if self._set_sample_period(dct_archive[minutes]) != 0:
            return 1
        if self._set_archive_period(minutes) != 0:
            return 1

        # memoire ne peut contenir qu'1 intervalle d'archivage => effacement
        if self.__reset_archive() != 0:
            logger.error("Issue to set Archive interval (%imin / %is)",
                         self._get_archive_interval(),
                         self._get_sample_period())
            return 1

        logger.info("Archive interval = %imin - Sample period = %is",
                    self._get_archive_interval(), self._get_sample_period())
        return 0

    def _set_archive_period(self, minutes):
        """
        Set the archive interval to n minutes.
        """
        #self.__clear_receive_buffer()
        data = struct.pack("<B", minutes)
        if self._cmd('SAP', data):
            logger.debug('Archive interval set')
            return 0
        else:
            logger.debug('Issue to set archive interval')
            return 1

    def _get_archive_interval(self):
        """
        Gets the archive interval in minutes or None if command fails.
        """
        buf = self.__read_link(2, 1, 0x3c)
        if buf is None:
            return None
        else:
            return struct.unpack("<B", buf)[0]

    def _get_sample_period(self):
        """
        Returns sample period in seconds or None if command fails.
        """
        buf = self.__read_link(2, 1, 0x3a)
        if buf is None:
            return None
        else:
            return 256 - struct.unpack("<B", buf)[0]

    def _set_sample_period(self, seconds):
        """
        Set the time interval between samples of the sensor image in
        the WeatherLink to 'n' seconds.  At each
        sampling instant the sensor image is sampled and variables that
        are "averaged" are accumulated.  The maximum number of samples in
        an interval is 255.
        """
        #self.__clear_receive_buffer()
        data = struct.pack("<B", 256-seconds)
        if self._cmd("SSP", data):
            logger.debug('Sample period set :%is.', seconds)
            return 0
        else:
            logger.debug('Issue to set sample period to %i', seconds)
            return 1

    def __read_link(self, nibbles, bank, addr):
        """
        Lecture de 'n' nibbles dans la memoire du processeur LINK a partir de
        'address' dans la banque
        """
        """
        if nibbles > 8:
            raise Exception("nibbles > 8")
        """
        data = struct.pack("<BBB", bank, addr, nibbles-1)
        if self._cmd("RRD", data):
            buf_len = int(float(nibbles)/2+0.5)
            buf = self.port.read(size=buf_len)
            return buf
        else:
            return None

    def __read_mem(self, nibbles, bank, addr):
        """
        Lecture memoire du processeur de la station
        """
        if bank == 1:
            b = 4
        else:
            b = 2

        nib = (nibbles << 4) | b
        data = struct.pack("<BB", nib, addr)

        if self._cmd("WRD", data):
            buf_len = int(float(nibbles)/2+0.5)
            return self.port.read(size=buf_len)
        else:
            return None

    def __write_mem(self, nibbles, bank, addr, data):
        """
        Ecrit dans la memoire de la station
        """
        b = 2
        if bank == 0:
            b = 1
        elif bank == 1:
            b = 3

        nib = (nibbles << 4) | b
        addr = struct.pack("<BB", nib, addr)
        if self._cmd("WWR ", addr, data):
            logger.info('Ecriture memoire station OK')
            return 0
        else:
            logger.warn('Ecriture memoire station echouee')

    def get_time(self):
        """ Lire l'heure de la station et la renvoie sous la forme d'un
        timetuple
        """
        #self.__clear_receive_buffer()

        buf = self.__read_mem(2, 1, 0xC8)
        D = struct.unpack("<B", buf)
        D = self._base.encode(D[0])

        buf = self.__read_mem(1, 1, 0xCA)
        M = int(struct.unpack("<B", buf)[0])

        buf = self.__read_mem(2, 1, 0xBE)
        h = struct.unpack("<B", buf)[0]
        h = self._base.encode(h)

        buf = self.__read_mem(2, 1, 0xC0)
        m = struct.unpack("<B", buf)[0]
        m = self._base.encode(m)

        buf = self.__read_mem(2, 1, 0xC2)
        s = struct.unpack("<B", buf)[0]
        s = self._base.encode(s)
        str_date = '%i-%02i-%02i - %02i:%02i:%02i' \
            % (date.today().year, M, D, h, m, s)
        logger.info('Heure console : %s', str_date)
        self.fields['arcDateStation'] = str_date
        return time.strptime(str_date, '%Y-%m-%d - %H:%M:%S')

    def _correct_fields(self):
        """
        Transforme les valeurs de champs aberrantes en None
        """

        # Vent
        lst_wind = ['WindDir', 'WindSpeed']
        for p in lst_wind:
            try:
                if self.fields[p] <= -32767 or self.fields[p] >= 32767:
                    self.fields[p] = None
            except KeyError:
                continue
        try:
            if self.fields['domWindDir'] == 255:
                self.fields['domWindDir'] = None
            if self.fields['avgWindSpeed'] <= 0:
                self.fields['WindDir'] = None
        except KeyError:
            pass
        # Humidite
        lst_hum = ['HumOut', 'HumIn', 'arcHumOut', 'arcHumIn']
        for p in lst_hum:
            try:
                if self.fields[p] in [-128, 128]:
                    self.fields[p] = None
            except KeyError:
                continue

        # Temperatures
        lst_tmp = ['TempIn', 'TempOut', 'loTempOut', 'hiTempOut', 'avgTempOut',
                   'avgTempIn']
        for p in lst_tmp:
            try:
                if self.fields[p] <= -3276 or self.fields[p] >= 3276:
                    self.fields[p] = None
            except KeyError:
                continue

    def set_time(self, date_set=None):
        """
        Fixe l'heure et la date de la station à partir d'un timetuple
        (traduction de la fonction "SetStationTime")
        """
        rep = 1
        if date_set is None:
            date_set = time.gmtime()

        if self.__disable_timer() != 0:
            logger.warn('Maj heure station impossible')
            return rep

        # date de la station
        day = (date_set.tm_mday // 10) * 16 + date_set.tm_mday % 10
        data = struct.pack("<BB", day, date_set.tm_mon)

        if self._cmd('WWR', '\x43', '\xc8', data):
            logger.debug('Date station mise a jour (%i/%02i/%02i)',
                         date_set.tm_year, date_set.tm_mon, date_set.tm_mday)
        else:
            logger.warn('Erreur dans maj date station')
            return rep

        # heure de la station
        hour = (date_set.tm_hour // 10) * 16 + date_set.tm_hour % 10
        m = (date_set.tm_min // 10) * 16 + date_set.tm_min % 10
        sec = (date_set.tm_sec // 10) * 16 + date_set.tm_sec % 10
        data = struct.pack("<3B", hour, m, sec)
        if self._cmd('WWR', "\x63", "\xbe", data):
            logger.debug('Heure station mise a jour (%02i:%02i)',
                         date_set.tm_hour, date_set.tm_min)
        else:
            self.__enable_timer()
            logger.warn('Erreur maj Heure station')
            return rep

        #Set last archive time so next archive entry's will occur on the hour.
        if not self.__put_last_archive_time(date_set.tm_hour, date_set.tm_min):
            self.__enable_timer()
            return rep

        if self.__enable_timer() == 0:
            logger.info('Date et heure console mise a jour')
            rep = 0
        else:
            logger.warn('Probleme de timer')
        return rep

    def __clear_receive_buffer(self):
        """
        Vide le flux de communication de la station
        (attend qu'il s'arrete)
        """
        self.port.flushInput()
        self.port.flushOutput()
        time.sleep(.1)

    def __disable_timer(self):
        """
        Desactive le timer de l'archive
        """
        rep = 1
        #self.__clear_receive_buffer()
        if self._cmd('DBT'):
            logger.debug('Timer disabled')
            rep = 0
        return rep

    def __enable_timer(self):
        """
        Active le timer de l'archive
        """
        rep = 1
        #self.__clear_receive_buffer()
        if self._cmd('EBT'):
            rep = 0
            logger.debug('Timer enabled')
            return 0
        return rep

    def parse(self):
        """
        Recupere les donnees de la station (RTC + archive)
        """
        self.fields.clear()
        self._get_sensor_fields()  # lecture des donnees du capteur
        self._get_archive_fields()  # letcure de l'archive
        self.get_time()
        # Conversion des données aberrantes en None
        self._correct_fields()

    def __reset_archive(self):
        """
        Efface la memoire de la console
        """
        if self.__disable_timer() != 0:
            return 1

        #self.__clear_receive_buffer()
        tmp = struct.pack("<5B", 0, 0, 0, 0, 0)
        #ecrit dans link processor memory : \x17 => bank 1 , nibbles 8
        if self._cmd('RWR', '\x17', tmp):
            logger.debug('Effacement memoire console : etape 1 OK')
        else:
            logger.error('Probleme lors effacement memoire console (1)')
            self.__enable_timer()
            return -1

        #self.__clear_receive_buffer()
        # Do read command to set data lines in input state.
        # So P67InOutFlag is in correct state.
        tmp = struct.pack("<2H", 0, 0)
        if self._cmd("SRD", tmp):
            logger.debug('Effacement memoire console : etape 2 OK')
        else:
            logger.error('Probleme lors effacement memoire console (2)')
            self.__enable_timer()
            return -1
        buf = self.port.read(size=3)
        if not wmIIcrc.verify(buf):
            logger.error("Bad crc checking")
            raise Exception("bad CRC Checking")

        # Set the WrapFlag, P67InOutFlag, and TimeOutFlag to 0.
        tmp = struct.pack("<B", 0)
        # ecrit dans link processor memory (bank 0 , 1 nibble ?), start at 42h
        if self._cmd('RWR', "\x00", "\x42", hex(0)):
            logger.debug('Effacement memoire console : etape 3 OK')
        else:
            logger.error('Probleme lors effacement memoire console (3)')
            return -1

        #if self.set_time() != 0:
        if self.__put_last_archive_time:
            logger.debug('Effacement memoire console : etape 4 OK')
        else:
            logger.error('Probleme lors effacement memoire console (4)')
            self.__enable_timer()
            return -1


        if self.__enable_timer() != 0:
            logger.error('Probleme lors effacement memoire console (5)')
            return -1
        else:
            logger.debug('Effacement memoire console : etape 5 OK')

        logger.info('Memoire console meteo effacee')
        return 0

    def __put_last_archive_time(self, hour=None, minutes=None):
        """
        Adjust last archive time so it is a multiple of the archive period.
        """
        #self.__clear_receive_buffer()
        arc_period = int(self._get_archive_interval())
        if arc_period is None:
            return False

        if hour is None or minutes is None:
            cur_time = self.get_time()
            hour, minutes = cur_time.tm_hour, cur_time.tm_min

        self.__clear_receive_buffer()
        logger.debug('Archive Time to adjust (%i:%02i)', hour, minutes)

        last_arc_time = (hour * 60 + minutes) // arc_period
        #lastArchiveTime *= arc_period
        last_arc_time = struct.pack("<H", last_arc_time)
        #if self._cmd("RWR", "\x13", "\x48", lastArchiveTime) == 0:
        # heure de la derniere archive : \x48 ou \x54 ?
        if self._cmd("RWR", "\x13", "\x54", last_arc_time):
            logger.debug('Archive Time adjusted (%i:%02i)', hour, minutes)
            return True
        else:
            logger.error('Issue adjusting archive time')
            return False
