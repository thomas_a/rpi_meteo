#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Davis Vantage Pro and Pro2 Service

Abstract:
Allows data query of Davis Vantage Pro and Pro2 devices via serial port
interface.  The primary implemented serial commands supported are LOOP and
DMPAFT.

The LOOP command can aquire all real-time data points. The DMPAFT command is
used to aquire periodic high/low data.

All data is returned in a dict structure with value/key pairs. Periodic data is
only captured once per period. When not active, the keys for periodic data are
not present in the results.

Author: Patrick C. McGinty (pyweather@tuxcoder.com)
Date: 2010-06-025

Original Author: Christopher Blunck (chris@wxnet.org)
Date: 2006-03-27
'''

from __future__ import absolute_import

import logging
import serial
import struct
import time

from weather.stations._struct import Struct
from weather.stations.davis import NoDeviceException
from rpi_meteo.crc import CRC
from rpi_meteo.base import Base

log = logging.getLogger("rpi_log."+__name__)

# public interfaces for module
__all__ = ['VantagePro', 'NoDeviceException']

READ_DELAY = 2
BAUD = 19200


def log_raw(msg, raw):
    """
    Affiche dans le log un message hexadécimal
    """
    log.debug(msg + ': ' + raw.encode('hex'))

# --------------------------------------------------------------------------- #


class LoopStruct(Struct):
    '''
    For unpacking data structure returned by the 'LOOP' command. this structure
    contains all of the real-time data that can be read from the Davis Vantage
    Pro.
    '''
    FMT = (('LOO', '3s'), ('BarTrend', 'B'), ('PacketType', 'B'),
           ('NextRec', 'H'), ('Pressure', 'H'), ('TempIn', 'H'),
           ('HumIn', 'B'), ('TempOut', 'H'), ('WindSpeed', 'B'),
           ('WindSpeed10Min', 'B'), ('WindDir', 'H'), ('ExtraTemps', '7s'),
           ('SoilTemps', '4s'), ('LeafTemps', '4s'), ('HumOut', 'B'),
           ('HumExtra', '7s'), ('RainRate', 'H'), ('UV', 'B'),
           ('SolarRad', 'H'), ('RainStorm', 'H'), ('StormStartDate', 'H'),
           ('RainDay', 'H'), ('RainMonth', 'H'), ('RainYear', 'H'),
           ('ETDay', 'H'), ('ETMonth', 'H'), ('ETYear', 'H'),
           ('SoilMoist', '4s'), ('LeafWetness', '4s'), ('AlarmIn', 'B'),
           ('AlarmRain', 'B'), ('AlarmOut', '2s'), ('AlarmExTempHum', '8s'),
           ('AlarmSoilLeaf', '4s'), ('BatteryStatus', 'B'),
           ('BatteryVolts', 'H'), ('ForecastIcon', 'B'),
           ('ForecastRuleNo', 'B'), ('SunRise', 'H'), ('SunSet', 'H'),
           ('EOL', '2s'), ('CRC', 'H'),)

    def __init__(self):
        Struct.__init__(self, self.FMT, '=')

    def _post_unpack(self, items):
        """
        Mise en forme des donnees LOOP + conversion en None quand pas de donnee
        """
        # Temperatures
        lst_p = ['TempIn', 'TempOut']
        for param in lst_p:
            if items[param] <= -32767 or items[param] >= 32767:
                items[param] = None
            else:
                items[param] /= 10.

        # Wind
        lst_wind = ['WindSpeed10Min', 'WindSpeed']
        for param in lst_wind:
            items[param] = None if items[param] == 255 else items[param]
        if items['WindDir'] == 32767:
            items['WindDir'] = None

        items['Pressure'] /= 1000.0
        if items['RainRate'] == 65535:
            items['RainRate'] = None
        else:
            items['RainRate'] /= 100.0

        # rain totals
        for param in ['RainStorm', 'RainDay', 'RainMonth', 'RainYear']:
            items[param] /= 100.0
        items['StormStartDate'] = \
            self._unpack_storm_date(items['StormStartDate'])

        # evapotranspiration totals
        items['ETDay'] /= 1000.0
        items['ETMonth'] /= 100.0
        items['ETYear'] /= 100.0

        # humidite + soil moisture + leaf wetness
        items['SoilMoist'] = struct.unpack('4B', items['SoilMoist'])
        items['LeafWetness'] = struct.unpack('4B', items['LeafWetness'])

        # Solar Rad
        param = 'SolarRad'
        items[param] = None if items[param] == 32767 else items[param]

        lst_p = ['UV', 'LeafWetness', 'LeafTemps', 'SoilTemps', 'SoilMoist',
                 'BarTrend', 'HumOut', 'HumIn']
        for param in lst_p:
            items[param] = None if items[param] == 255 else items[param]

        # battery statistics
        items['BatteryVolts'] = items['BatteryVolts'] * 300 / 512.0 / 100.0
        # sunrise / sunset
        items['SunRise'] = self._unpack_time(items['SunRise'])
        items['SunSet'] = self._unpack_time(items['SunSet'])
        log.debug('Donnees LOOP mises en forme en memoire')
        return items

    @staticmethod
    def _unpack_time(val):
        '''
        given a packed time field, unpack and return "HH:MM" string.
        '''
        # format: HHMM, and space padded on the left.ex: "601" is 6:01 AM
        return "%02d:%02d" % divmod(val, 100)  # covert to "06:01"

    @staticmethod
    def _unpack_storm_date(date):
        '''
        given a packed storm date field, unpack and return 'YYYY-MM-DD' string.
        '''
        year = (date & 0x7f) + 2000        # 7 bits
        day = (date >> 7) & 0x01f         # 5 bits
        month = (date >> 12) & 0x0f         # 4 bits
        return "%s-%s-%s" % (year, month, day)

# --------------------------------------------------------------------------- #


class _ArchiveStruct(object):
    '''
    common features for both Rev.A and Rev.B structures.
    '''
    def __init__(self):
        super(_ArchiveStruct, self).__init__(self.FMT, '=')

    def _post_unpack(self, items):
        """
        Met en forme les donnees DUMP
        """
        lst_tmp = ['avgTempIn', 'avgTempOut', 'loTempOut', 'hiTempOut']
        for param in lst_tmp:
            if items[param] <= -32767 or items[param] >= 32767:
                items[param] = None
            else:
                items[param] /= 10.
        # Vent
        lst_wind = ['avgWind', 'hiWind']
        for param in lst_wind:
            items[param] = None if items[param] == 255 \
                else int(items[param] * 22.5)

        lst_wind = ['hiWindDir', 'avgWindDir']
        for param in lst_wind:
            if items[param] == 32767 or items[param] == 255:
                items[param] = None
            else:
                items[param] = int(items[param] * 22.5)

        if items['avgWind'] <= 0 or items['avgWind'] is None:
            items['avgWindDir'] = None
            items['hiWindDir'] = None

        # Humidite + Leaf
        lst_p = ['arcHumOut', 'arcHumIn', 'arcLeafWetness', 'arcLeafTemps',
                 'hiUV']
        for param in lst_p:
            items[param] = None if items[param] == 255 else items[param]

        lst_sr = ['SolarRad', 'arcSolarRad', 'hiSolarRad']
        for param in lst_sr:
            try:
                items[param] = None if items[param] == 32767 else items[param]
            except KeyError:
                continue

        # date
        vals = self._unpack_date_time(items['arcDateStamp'],
                                      items['arcTimeStamp'])
        items.update(zip(('Year', 'Month', 'Day', 'Hour', 'Min'), vals))
        items['arcDateStation'] = '%04i-%02i-%02i %02i:%02i' \
            % (items['Year'], items['Month'], items['Day'], items['Hour'],
               items['Min'])

        items['arcPressure'] /= 1000.0
        # TODO : faire la meme chose pour hiUV ?
        items['arcUV'] = items['arcUV']/10. if items['arcUV'] != 255 else None
        items['ETHour'] /= 1000.0
        items['arcSoilTemps'] = \
            tuple(t-90 for t in struct.unpack('4B', items['arcSoilTemps']))
        items['arcExtraHum'] = struct.unpack('2B', items['arcExtraHum'])
        items['arcSoilMoist'] = struct.unpack('4B', items['arcSoilMoist'])
        log.debug('Donnees DUMP mises en forme en memoire')
        return items

    @staticmethod
    def _unpack_date_time(date, heure):
        """
        Renvoie la date de la station
        """
        day = date & 0x1f                     # 5 bits
        month = (date >> 5) & 0x0f              # 4 bits
        year = ((date >> 9) & 0x7f) + 2000     # 7 bits
        hour, min_ = divmod(heure, 100)
        return (year, month, day, hour, min_)

# --------------------------------------------------------------------------- #


class _ArchiveAStruct(_ArchiveStruct, Struct):
    """
    Structure des donnnees archivees (Version A)
    """
    FMT = (('arcDateStamp', 'H'), ('arcTimeStamp', 'H'), ('avgTempOut', 'H'),
           ('hiTempOut', 'H'), ('loTempOut', 'H'), ('arcRainRate', 'H'),
           ('hiRainRate', 'H'), ('arcPressure', 'H'), ('arcSolarRad', 'H'),
           ('arcWindSamps', 'H'), ('avgTempIn', 'H'), ('arcHumIn', 'B'),
           ('arcHumOut', 'B'), ('avgWind', 'B'), ('hiWind', 'B'),
           ('hiWindDir', 'B'), ('avgWindDir', 'B'), ('arcUV', 'B'),
           ('ETHour', 'B'), ('unused', 'B'), ('arcSoilMoist', '4s'),
           ('arcSoilTemps', '4s'), ('arcLeafWetness', '4s'),
           ('arcExtraTemps', '2s'), ('arcExtraHum', '2s'), ('ReedClosed', 'H'),
           ('ReedOpened', 'H'), ('unused', 'B'),)

    def _post_unpack(self, items):
        items = super(_ArchiveAStruct, self)._post_unpack(items)
        items['arcLeafWetness'] = struct.unpack('4B', items['arcLeafWetness'])
        items['arcExtraTemps'] = \
            tuple(t-90 for t in struct.unpack('2B', items['arcExtraTemps']))
        return items

# --------------------------------------------------------------------------- #


class _ArchiveBStruct(_ArchiveStruct, Struct):
    """
    Structure des donnnees archivees (Version B)
    """
    FMT = (('arcDateStamp', 'H'), ('arcTimeStamp', 'H'), ('avgTempOut', 'H'),
          ('hiTempOut', 'H'), ('loTempOut', 'H'), ('arcRainRate', 'H'),
          ('hiRainRate', 'H'), ('arcPressure', 'H'), ('arcSolarRad', 'H'),
          ('arcWindSamps', 'H'), ('avgTempIn', 'H'), ('arcHumIn', 'B'),
          ('arcHumOut', 'B'), ('avgWind', 'B'), ('hiWind', 'B'),
          ('hiWindDir', 'B'), ('avgWindDir', 'B'), ('arcUV', 'B'),
          ('ETHour', 'B'), ('hiSolarRad', 'H'), ('hiUV', 'B'),
          ('ForecastRuleNo', 'B'), ('arcLeafTemps', '2s'),
          ('arcLeafWetness', '2s'), ('arcSoilTemps', '4s'), ('RecType', 'B'),
          ('arcExtraHum', '2s'), ('arcExtraTemps', '3s'),
          ('arcSoilMoist', '4s'),)

    def _post_unpack(self, items):
        items = super(_ArchiveBStruct, self)._post_unpack(items)
        items['arcLeafTemps'] = \
            tuple(t-90 for t in struct.unpack('2B', items['arcLeafTemps']))
        items['arcLeafWetness'] = struct.unpack('2B', items['arcLeafWetness'])
        items['arcExtraTemps'] = \
            tuple(t-90 for t in struct.unpack('3B', items['arcExtraTemps']))
        return items

# --------------------------------------------------------------------------- #

# simple data structures
struct_dmp = Struct((('Pages', 'H'), ('Offset', 'H'), ('CRC', 'H')), order='=')
struct_pg_dmp = Struct((('Index', 'B'), ('Records', '260s'), ('unused', '4B'),
                        ('CRC', 'H')), order='=')

# init structure classes
struct_loop = LoopStruct()
ArchiveAStruct = _ArchiveAStruct()
ArchiveBStruct = _ArchiveBStruct()


##############################################################################
#|--------------------------------------------------------------------------|#
#|--------------------------------------------------------------------------|#
#|                     API for the Davis Vantage Pro                        |#
#|--------------------------------------------------------------------------|#
#|--------------------------------------------------------------------------|#
##############################################################################

VProCRC = CRC()


class VantagePro(object):
    '''
    A class capable of reading raw (binary) weather data from a
    vantage pro console and parsing it into usable scalar
    (integer/long/real) values.

    The data read from the console is in binary format. The data is in
    least-ordered nybble strategy, and must be read with correct sizes and
    offsets for proper byte ordering.
    '''

    # device reply commands
    _WAKE_ACK = '\n\r'
    _ACK = '\x06'
    _ESC = '\x1b'
    _OK = '\n\rOK\n\r'
    _NOK = '\x21'

    _RETRY = 5

    # archive format type, unknown
    _ARCHIVE_REV_B = None

    def __init__(self, device, log_interval=5):
        try:
            self.port = serial.Serial(device, BAUD, timeout=READ_DELAY)
        except (serial.SerialException, OSError):
            self.port = None
            str_msg = 'Access denied on %s' % device
            log.error(str_msg)
            return None

        self._archive_time = (0, 0)
        self.fields = None
        self._base = Base()

        if not self._wakeup():
            raise NoDeviceException('Unable to wake-up station')

        # set logging interval
        if self._set_archive_interval(log_interval) != 0:
            raise NoDeviceException('Can not access weather station')
        # passe en mode acquisition donnees meteo
        self.port.write("RXTEST\n")

        # signale connection avec station (fait clignoter la lumiere)
        self._cmd("LAMPS 1", expected=self._OK)
        time.sleep(1)
        self._cmd("LAMPS 0", expected=self._OK)
        log.info('Console Vantage Pro 2 prete')

    def _test(self):
        """
        Test communication avec console pour s'assurer qu'elle est active
        => sinon wake-up
        """
        self.__clear_receive_buffer()
        self.port.write('TEST\n')
        expected = '\n\rTEST\n\r'
        response = self.port.read(len(expected))
        log_raw('read', response)
        if response == expected:
            log.info('Test console : OK')
            return True
        else:
            log.warning('Test console : Fail')
            return False

    def __del__(self):
        '''
        close serial port when object is deleted.
        '''
        try:
            self.port.close()
        except AttributeError:
            log.error('Fermeture station => port inexistant')
        log.info('Station meteo fermée')

    def __clear_receive_buffer(self):
        """
        Vide le flux de communication de la station
        (attend qu'il s'arrete)
        """
        self.port.flushInput()
        self.port.flushOutput()
        time.sleep(.1)

    def _use_rev_b_archive(self, records, offset):
        '''
        return True if weather station returns Rev.B archives
        '''
        # if pre-determined, return result
        if type(self._ARCHIVE_REV_B) is bool:
            return self._ARCHIVE_REV_B
        # assume, B and check 'RecType' field
        data = ArchiveBStruct.unpack_from(records, offset)
        if data['RecType'] == 0:
            log.info('detected archive rev. B')
            self._ARCHIVE_REV_B = True
        else:
            log.info('detected archive rev. A')
            self._ARCHIVE_REV_B = False

        return self._ARCHIVE_REV_B

    def _check_ack(self):
        """
        Check Acknowledgement
        """
        resp = self.port.read()
        if resp == '':
            raise NoDeviceException("Timeout waiting for ack")

        ack = struct.unpack("<B", resp)
        if ack[0] == 0x06:
            log.debug("Response OK!")
        if ack[0] == 0x21:
            log.debug("Response NOK!")
            return False
        else:
            msg = "Response was not NOK, but not ACK either..."
            log.debug(log.debug)
            raise NoDeviceException(msg)
        return True

    def _set_archive_interval(self, minutes):
        """
        Definit l'interval d'enregistrement et la periode d'enregistrement
        correspondante
        """

        # verifie la frequence d'archivage actuelle et modifie si necessaire
        interval = self._get_archive_interval()
        if interval == minutes:
            log.info("Archive interval already = %i min.", minutes)
            return 0
        elif interval is None:
            log.warning("Issue to get archive interval : get %s", interval)
            return 1
        else:
            # definition intervalle archivage
            log.debug("Setting archive interval to %i min.", minutes)
            if self._cmd('SETPER', self._WAKE_ACK, minutes) == 0:
                log.info("Archive interval = %i min.", minutes)
                self._cmd("NEWSETUP", self._ACK)
            else:
                log.warning("Issue to set archive interval to %i min.",
                            minutes)
                return 1

            self.__reset_archive()
            log.info("Intervalle d'archivage = %i min.", minutes)
        return 0

    def _get_archive_interval(self):
        """
        Returns archive period in minutes or None if command fails.
        """
        arctime = None
        if self._cmd("EERD 2D 1", expected=self._OK) == 0:
            ack = self.port.read(len(self._ACK))
            arctime = self.port.read()
            log.debug('Actual archive interval = %s', arctime)

        return self._base.decode(arctime)

    def _wakeup(self):
        '''
        issue wakeup command to device to take out of standby mode.
        '''
        if self._test():
            log.debug('Console already awake')
            return True

        for i in xrange(self._RETRY):
            time.sleep(i/float(self._RETRY))
            # Clear out any pending input or output characters:
            self.__clear_receive_buffer()
            log.debug("send: WAKEUP (%i/%i)", i+1, self._RETRY)
            self.port.write('\n')                    # wakeup device
            ack = self.port.read(len(self._WAKE_ACK))  # read wakeup string
            log_raw('read', ack)
            if ack == self._WAKE_ACK:
                log.debug('Console now awake')
                return True

        log.warning('Can not wake-up weather station')
        return False

    def _cmd(self, cmd, expected='', *args):
        '''
        write a single command, with variable number of arguments. after the
        command, the device must return ACK
        '''
        if not self._wakeup():
            return 1

        if expected == '':
            expected = self._ACK

        if args:
            cmd = "%s %s" % (cmd, ' '.join(str(a) for a in args))

        for i in xrange(self._RETRY):
            time.sleep(i/float(self._RETRY))
            self.__clear_receive_buffer()
            log.debug("send: %s (%i/%i)", cmd.strip(), i + 1, self._RETRY)
            self.port.write(cmd + '\n')
            response = self.port.read(len(expected))  # read OK
            log_raw('read', response)
            if response == expected:
                log.debug('%s sent (%i) => OK', cmd, i + 1)
                return 0
            elif response == self._NOK:
                log.debug('%s sent (%i) => NOK response', cmd, i + 1)

        log.warning('%s sent => waiting for %s : Fail', cmd, expected)
        return 1

    def _loop_cmd(self):
        '''
        reads a raw string containing data read from the device
        provided (in /dev/ttyX) format. all reads are non-blocking.
        '''
        raw = None
        if self._cmd('LOOP', self._ACK, 1) == 0:
            raw = self.port.read(struct_loop.size)  # read data
            log_raw('read', raw)
        return raw

    def _dmpaft_cmd(self, time_fields):
        '''
        issue a command to read the archive records after a known time stamp.
        '''
        records = []
        # convert time stamp fields to buffer
        tbuf = struct.pack('2H', *time_fields)

        # 1. send 'DMPAFT' cmd
        if self._cmd('DMPAFT') != 0:
            log.error('Commande DMPAFT mal transmise')

        # 2. send time stamp + crc
        crc = VProCRC.get(tbuf)
        crc = struct.pack('>H', crc)             # crc in big-endian format
        log_raw('send', tbuf + crc)
        self.port.write(tbuf + crc)           # send time stamp + crc
        ack = self.port.read(len(self._ACK))     # read ACK
        log_raw('read', ack)
        if ack != self._ACK:
            return              # if bad ack, return

        # 3. read pre-amble data
        raw = self.port.read(struct_dmp.size)
        log_raw('read', raw)
        if not VProCRC.verify(raw):           # check CRC value
            log_raw('send ESC', self._ESC)
            self.port.write(self._ESC)         # if bad, escape and abort
            return
        log_raw('send ACK', self._ACK)
        self.port.write(self._ACK)             # send ACK

        # 4. loop through all page records
        dmp = struct_dmp.unpack(raw)
        log.info('reading %d pages, start offset %d', dmp['Pages'],
                 dmp['Offset'])
        for i in xrange(dmp['Pages']):
            # 5. read page data
            raw = self.port.read(struct_pg_dmp.size)
            log_raw('read', raw)
            if not VProCRC.verify(raw):       # check CRC value
                log_raw('send ESC', self._ESC)
                self.port.write(self._ESC)  # if bad, escape and abort
                return
            log_raw('send ACK', self._ACK)
            self.port.write(self._ACK)  # send ACK

            # 6. loop through archive records
            page = struct_pg_dmp.unpack(raw)
            offset = 0  # assume offset at 0
            if i == 0:
                offset = dmp['Offset'] * ArchiveAStruct.size
            while offset < ArchiveAStruct.size * 5:
                log.debug('page %d, reading record at offset %d',
                          page['Index'], offset)
                if self._use_rev_b_archive(page['Records'], offset):
                    a = ArchiveBStruct.unpack_from(page['Records'], offset)
                else:
                    a = ArchiveAStruct.unpack_from(page['Records'], offset)
                # 7. verify that record has valid data, and store
                if a['arcDateStamp'] != 0xffff and a['arcTimeStamp'] != 0xffff:
                    records.append(a)
                offset += ArchiveAStruct.size
        log.info('Read all pages')
        return records

    def _get_loop_fields(self):
        """
        Recupere les donnees transmises par la commande 'LOOP'
        """
        for i in xrange(3):
            raw = self._loop_cmd()  # read raw data
            crc_ok = VProCRC.verify(raw)
            if crc_ok:
                break                # exit loop if valid
            time.sleep(1)

        if not crc_ok:
            log.warning('Impossible de recuperer donnees LOOP')
            raise NoDeviceException('Can not access weather station')

        log.debug('Donnees LOOP transmis par station')
        return struct_loop.unpack(raw)

    def _get_archive_fields(self):
        '''
        returns a dictionary of fields from the newest archive record in the
        device. return None when no records are new.
        '''
        for i in xrange(3):
            records = self._dmpaft_cmd(self._archive_time)
            if records is not None:
                break
            time.sleep(1)

        if records is None:
            raise NoDeviceException('Can not access weather station')

        # find the newest record
        new_rec = None
        for r in records:
            new_time = (r['arcDateStamp'], r['arcTimeStamp'])
            if self._archive_time < new_time:
                self._archive_time = new_time
                new_rec = r
        log.debug('Donnees DMP tranmises par station')
        return new_rec

    def parse(self):
        '''
        read and parse a set of data read from the console.  after the
        data is parsed it is available in the fields variable.
        '''
        log.debug('Try to parse data...')
        fields = dict(self._get_loop_fields())
        fieldsA = self._get_archive_fields()
        if fieldsA is not None:
            fieldsA = dict(fieldsA)
            for i, v in fieldsA.iteritems():
                fields[i] = v

        # TODO : calcul ISS reception ?

        # set the fields variable the the values in the dict
        self.fields = fields

    def set_time(self, date):
        """
        Définir l'heure et la date de la station
        """
        self._wakeup()
        self.port.write("SETTIME\n")
        ack = self.port.read(len(self._ACK))
        buf = struct.pack("<bbbbbb", date.tm_sec, date.tm_min, date.tm_hour,
                          date.tm_mday, date.tm_mon, date.tm_year - 1900)
        crc = VProCRC.get(buf)
        buf = buf + struct.pack(">H", crc)
        self.port.write(buf)
        ack = self.port.read(len(self._ACK))  # read ACK
        if ack == self._ACK:
            log.info('Station time set')
            return 0
        else:
            log.warning('Pb. pour changer heure console')
            return 1

    def get_time(self):
        """
        Recupere l'heure et la date de la station
        """
        #self._wakeup()
        # ... request the time...
        if self._cmd('GETTIME') == 0:
            _buffer = self.port.read(8)
            (sec, minute, hr, day, mon, yr, unused_crc) = \
                struct.unpack("<bbbbbbH", _buffer)
            log.debug('Console : get time OK')
        else:
            log.warning('Console : fail to get time')
            return None
        """
        self.port.write('GETTIME\n')
        ack = self.port.read(len(self._ACK))
        # ... get the binary data. No prompt, only one try:
        _buffer = self.port.read(8)
        (sec, minute, hr, day, mon, yr, unused_crc) = \
            struct.unpack("<bbbbbbH", _buffer)
        """

        s_date = '%04i-%02i-%02i %02i:%02i:%02i' \
                 % (1900 + int(yr), mon, day, hr, minute, sec)
        log.info('Heure console : %s', s_date)
        try:
            date_vp2 = time.strptime(s_date, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            date_vp2 = None
            log.warning('Heure console : valeur aberrante [%s]', s_date)
        return date_vp2

    def __reset_archive(self):
        """
        Efface la memoire de la console (donnees archivees)
        """
        if self._cmd('CLRLOG') == 0:
            log.info('Memoire console videe')
        else:
            log.warning('Issue to erase console memory')
