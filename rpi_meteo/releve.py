#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Releve meteorologique recupere a partir d'une station meteo.
Les donnees d'interet sont selectionnees et les unites converties

Ce releve peut ensuite etre publie sur le web et dans un fichier
Created on Fri Mar  7 11:16:30 2014

@author: thomas_a
"""
import os
import csv
import logging
from math import ceil, floor

#import pandas as pd
from collections import OrderedDict

from weather.units import temp, pressure, wind

#log = logging.getLogger(__name__)
logger = logging.getLogger("rpi_log."+__name__)

class Releve(object):
    """
    Releve meteorologique compatible avec Davis Weather MOnitor II
    & Vantage PRO 2
    """

    def __init__(self, ids, model, path):
        self._id = ids
        if model == 'WeatherMII':
            self.model = 'wm2'
        elif model == 'VantagePro2':
            self.model = 'vp2'
        else:
            self.model = ''
        self.__path = path  # dossier en sortie
        self._lst_flds = ['utc_date_PC', 'arcDateStation',
                          'TempOut', 'TempIn', 'Pressure', 'RainRate',
                          'HumOut', 'HumIn',
                          'WindDir', 'WindSpeed', 'WindSpeed10Min',
                          'UV', 'SolarRad',
                          'avgTempOut', 'hiTempOut', 'loTempOut', 'avgTempIn',
                          'arcPressure', 'arcRainRate', 'hiRainRate',
                          'arcHumOut', 'arcHumIn',
                          'avgWind', 'hiWind', 'avgWindDir', 'hiWindDir',
                          'arcUV', 'hiUV', 'arcSolarRad', 'hiSolarRad',
                          'BatteryStatus', 'NextRec']

        # itinialise tous les champs du releve complet et RTC, avec None
        self.__flds = OrderedDict((k, None) for k in self._lst_flds)
        lst_flds_rtc = ['id', 'tmp', 'hum', 'pp', 'wdir', 'wsp', 'pr', 'date']
        self.rtc = OrderedDict((k, None) for k in lst_flds_rtc)
        self.rtc['id'] = self._id

    def __correct(self):
        """
        Corrige des valeurs impossibles
        """
        # HUMIDITE > 100%
        lst_tmp = ['HumOut', 'HumIn', 'arcHumOut', 'arcHumIn']
        for param in lst_tmp:
            try:
                if self.__flds[param] > 100:
                    self.__flds[param] = None
            except (KeyError, TypeError):
                continue

    def __convert(self):
        """
        Applique les changements d'unité et arrondie les valeurs
        """
        # TEMPERATURES
        lst_tmp = ['TempOut', 'TempIn', 'hiTempOut', 'loTempOut',
                   'avgTempIn', 'avgTempOut']
        for param in lst_tmp:
            try:
                self.__flds[param] = temp.fahrenheit_to_celsius(\
                                     self.__flds[param])
            except (KeyError, TypeError):
                continue
            else:
                # arrondi differentsi valeurs moyennes, hautes ou basse
                if param == 'hiTempOut':
                    self.__flds[param] = ceil(self.__flds[param]*10)/10
                elif param == 'loTempOut':
                    self.__flds[param] = floor(self.__flds[param]*10)/10
                else:
                    self.__flds[param] = round(self.__flds[param], 1)

        # correction de hiTempOut/loTempOut à partir T°c actuelle ou moyenne
        if None not in [self.__flds[k] for k in self.__flds if k in \
                        ['TempOut', 'hiTempOut', 'loTempOut', 'avgTempOut']]:
            self.__flds['hiTempOut'] = max(self.__flds['TempOut'],
                                           self.__flds['avgTempOut'],
                                           self.__flds['hiTempOut'])
            self.__flds['loTempOut'] = min(self.__flds['TempOut'],
                                           self.__flds['avgTempOut'],
                                           self.__flds['loTempOut'])

        # PRESSION
        lst_pr = ['Pressure', 'arcPressure']
        for param in lst_pr:
            try:
                self.__flds[param] = pressure.in32_to_mb(self.__flds[param])
                self.__flds[param] = round(pressure.mb_to_hpa( \
                                     self.__flds[param]), 1)
            except (KeyError, TypeError):
                continue

        # PLUVIO
        lst_pr = ['PrevRain', ]
        for param in lst_pr:
            try:
                self.__flds[param] = round(self.__flds[param] * 25.4, 1)
            except KeyError:
                continue

        # VENT
        try:
            self.__flds['WindSpeed'] = round(wind.mph_to_m_sec
                                             (self.__flds['WindSpeed']), 1)
        except (KeyError, TypeError):
            pass

    @property
    def fields(self):
        """
        Recuperer donnees du releve
        """
        return self.__flds

    @fields.setter
    def fields(self, values):
        """
        Les donnees de la station alimentent le releve
        """
        # affecte les donnees du releve
        for key in self.__flds.keys():
            try:
                self.__flds[key] = values[key]
            except KeyError:
                self.__flds[key] = None
        logger.debug('Donnees station => Releve')

        # affecte les donnees RTC
        if self.__flds['arcDateStation'] is None:
            date_station = self.__flds['utc_date_PC']
        else:
            date_station = self.__flds['arcDateStation']
            date_station = date_station.replace(' - ', ' ')
        logger.debug("Date releve : %s", date_station)

        # réinitialise relevé RTC
        for key in self.rtc.keys():
            self.rtc[key] = None
        self.rtc['id'] = self._id
        self.rtc['date'] = date_station
        logger.debug('Donnees RTC reinitialise')
        # si pas de donnee meteo, on quitte
        if values is None or len(values) <= 1:
            logger.warning('Aucune donnee meteo acquise')
            return None

        # conversion des unites
        self.__convert()

        # correction des valeurs aberrantes
        self.__correct()

        # selection des donnees RTC
        # equivalences entre clés rtc et clés station
        dct_rtc_station = {'tmp': 'TempOut', 'hum': 'HumOut', 'pp': 'RainRate',
                           'wdir': 'WindDir', 'wsp': 'WindSpeed',
                           'pr': 'Pressure'}
        for k in dct_rtc_station:
            try:
                self.rtc[k] = self.__flds[dct_rtc_station[k]]
            except KeyError:
                logger.debug('Donnee RTC manquante : %s', k)
                continue
        msg = [str(i) if i is not None else '' for i in self.rtc.values()]
        logger.debug("Data RTC: "+"| ".join(msg))

    def _calc_derived_fields(self):
        '''
        calculates the derived fields (those fields that are calculated)
        '''
        # convenience variables for the calculations below
        tmp = self.__flds['TempOut']
        hum = self.__flds['HumOut']
        #wind = self.__flds['WindSpeed']
        #wind10min = self.__flds['WindSpeed10Min']
        self.__flds['HeatIndex'] = temp.calc_heat_index(tmp, hum)
        #self.__flds['WindChill'] = temp.calc_wind_chill(tmp, wind, wind10min)
        self.__flds['DewPoint'] = temp.calc_dewpoint(tmp, hum)

    def to_file(self):
        """
        Ecrit les données dans un fichier texte
        """
        fichier = os.path.join(self.__path,
                               '%s_%s_%s.csv' %
                               (self._id, self.model, self.rtc['date'][:10]))
        logger.info('Ecriture data : %s', os.path.basename(fichier))
        if not os.path.exists(fichier):
            with open(fichier, mode='wb') as ofi:
                w_csv = csv.writer(ofi, delimiter=';')
                w_csv.writerow(self.__flds.keys())
                w_csv.writerow(self.__flds.values())
        else:
            with open(fichier, mode='ab') as ofi:
                w_csv = csv.writer(ofi, delimiter=';')
                w_csv.writerow(self.__flds.values())

        logger.info('%i donnees ecrites dans %s', len(self.__flds),
                    os.path.basename(fichier))
        return True
