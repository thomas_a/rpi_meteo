#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Classe générique pour les stations météorologiques

Created on Fri Jan  2 18:57:39 2015

@author: thomas_a
"""
import os
import logging
from datetime import datetime
import termios

from weather.stations.davis import NoDeviceException

from rpi_meteo.wmonitor2 import WeatherMII
from rpi_meteo.vantage_pro2 import VantagePro

logger = logging.getLogger("rpi_log."+__name__)


class UnknownStationException(Exception):
    """
    Exception rencontrées lors d'un modèle inconnu de stations meteo
    """
    pass


class Station(object):
    """
    Classe générique de station météorologique
    """
    _DATE_FMT = "%Y-%m-%d %H:%M:%S"
    def __init__(self, model, frequence):
        """
        Initialise une station d'un modèle donnée
        """
        if model not in ['WeatherMII', 'VantagePro2']:
            raise UnknownStationException('%s => Unknown station model', model)
        self.model = model

        lst_interval = [1, 5, 10, 15, 30, 60, 120]
        if frequence not in lst_interval:
            logger.error("Bad interval value : %i min.", frequence)
            raise Exception("Bad interval value")
        else:
            self.frequence = frequence

        self._console = None
        self._date = None
        self.connected = False

        self.check()

    def parse(self):
        """
        Recupere les donnees de la console
        """
        fields = dict()

        for i in range(3):
            logger.debug('Parsing weather data (%i/3)...', i)
            if not self.connected:
                self.check()

            if self.connected:
                try:
                    self._console.parse()
                    break
                except (NameError, AttributeError, NoDeviceException,
                        termios.error):
                    logger.warning('Communication with console lost')
                    self.connected = False
                    self._console = None
            else:
                logger.warning('Console disconnected')

        if self.connected:
            fields = self._console.fields
            logger.info('Weather data parsed')
        else:
            logger.error('Fail to parse weather data')

        return fields

    @property
    def port(self):
        """
        Recherche le port sur lequel est connecté la station
        """
        # boucle sur les ports à tester
        detected = False
        for i in range(4):
            # TODO : ajouter 'ttyAMA' dans liste des devices des que confusion
            # avec terminal serie sera levée
            for _device in ['ttyUSB', ]:
                str_port = '/dev/%s%i' % (_device, i)
                logger.debug('Checking %s device...', str_port)
                if not os.path.exists(str_port):
                    logger.debug('Checking %s => Failed', str_port)
                    continue
                else:
                    logger.info('Station detected on %s', str_port)
                    detected = True
                    break
            if detected:
                break
        if not detected:
            logger.warning('Aucune station detectee sur ports serie')
            self.connected = False
            return None

        # verification droit d'ecriture sur port
        if os.access(str_port, os.W_OK):
            logger.debug('Permissions OK sur %s', str_port)
        else:
            try:
                os.chmod(str_port, os.W_OK)
            except OSError:
                logger.error('Permission refusée sur %s', str_port)
                self.connected = False
                return None
            logger.debug('Permissions changées sur %s', str_port)
        return str_port

    def check(self):
        """
        Verifie la présence d'une station connectée au systeme avant d'essayer
        de récupérer les informations de cette station
        """
        # Connection à la station
        logger.debug('Tentative de Connection a station...')
        self.connected = False
        str_port = self.port
        if str_port is None:
            return False
        # Connection avec console de la station meteo
        self.connected = True
        self._console = None
        if self.model == 'WeatherMII':
            try:
                self._console = WeatherMII(device=str_port,
                                           log_interval=self.frequence)
            except NoDeviceException:
                self.connected = False
        elif self.model == 'VantagePro2':
            try:
                self._console = VantagePro(device=str_port,
                                           log_interval=self.frequence)
            except NoDeviceException:
                self.connected = False

        if self._console is not None and self.connected:
            logger.info('Station %s Connectée (port %s)', self.model, str_port)
        else:
            self.connected = False
            self._console = None
            logger.warning('Pas de station %s connectée', self.model)
        return self.connected

    @property
    def date(self):
        """
        Recuperer heure de la console
        """
        logger.debug('Heure station...')
        self._date = None
        if self.connected:
            try:
                self._date = datetime(*self._console.get_time()[:6])
            except (ValueError, NoDeviceException):
                logger.warning('Pb. avec heure console')
                return None
            else:
                logger.debug('Heure station : %s',
                             self._date.strftime(self._DATE_FMT))
        else:
            logger.warning('Heure console inconnue : station non connectée')

        return self._date

    @date.setter
    def date(self, date):
        """
        Fixer heure de la console à partir d'une variable de type datetime
        """
        if type(date) != datetime:
            msg = 'Wrong Type to set station date : %s', str(date)
            logger.error(msg)
            self._date = None
            raise TypeError(msg)

        if self.connected:
            logger.debug('Setting time station...')
            timet = date.timetuple()
            self._console.set_time(timet)
            self._date = date
        else:
            logger.warning('Setting time station aborted : not connected')
            self._date = None

    def __del__(self):
        self._date = None
        self._console = None
