#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  7 11:38:51 2014

@author: thomas_a
"""
import os
import sys
import logging
import logging.config

from config import config
print(sys.argv[0])
pathname = os.path.dirname(sys.argv[0])
os.chdir(pathname)

# verification de la config du Raspberry
cfg = config('rpi_meteo.cfg')

## creation du log
logging.config.fileConfig('logging.cfg')
#logging.basicConfig(filename=os.path.join(cfg.dirs['out'], 'rpi.log'))
log = logging.getLogger('rpi_log')

# verification des dependances
try:
    from weather import __package__
    from pexpect import __package__
except ImportError:
    log.error('Certaines librairies Python ne sont pas installees')
    raise ImportError
