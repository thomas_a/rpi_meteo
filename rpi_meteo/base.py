#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Encode a number in Base X or decode a Base X string into a number
Created on Fri Mar  7 12:35:09 2014

@author: thomas_a
"""


class Base(object):
    """
    Encode a number in Base X or decode a Base X string into a number
    """
    ALPHABET = ""

    def __init__(self, ALPHABET="0123456789abcdef"):
        self.ALPHABET = ALPHABET

    def encode(self, num):
        """
        Encode a number in Base X
        - num: The number to encode
        - alphabet: The alphabet to use for encoding
        """
        if num == 0:
            return int(self.ALPHABET[0])
        arr = []
        _base = len(self.ALPHABET)
        while num:
            rem = num % _base
            num = num // _base
            arr.append(str(self.ALPHABET[rem]))
        arr.reverse()
        return int(''.join(arr))

    def decode(self, string):
        """
        Decode a Base X encoded string into the number

        Arguments:
        - string: The encoded string
        (self.ALPHABET: The alphabet to use for encoding)
        """
        string = string.lower()
        _base = len(self.ALPHABET)
        strlen = len(string)
        num = 0

        idx = 0
        for char in string:
            power = (strlen - (idx + 1))
            num += self.ALPHABET.index(char) * (_base ** power)
            idx += 1
        return num

