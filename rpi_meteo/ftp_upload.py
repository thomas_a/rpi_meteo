#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
FtpUpload

Upload files via FTP based on their content changing.

Ned Batchelder
http://www.nedbatchelder.com

source : http://www.nedbatchelder.com/code/modules/FtpUpload.py
"""

import ftplib
import socket
import pickle
import md5
import os
import time

import logging
import path         # http://www.jorendorff.com/articles/python/path

__version__ = '1.0a'
__all__ = ['FtpUpload']

logger = logging.getLogger("rpi_log."+__name__)
WAIT_TIME = 0.01 # delai entre commandes
TIMEOUT = 3 # timeout

class EzFtp(object):
    """
    A simplified interface to ftplib.

    Lets you use full pathnames, with server-side
    directory management handled automatically.
    """
    def __init__(self, host, username, password, host_path=''):
        self.__ftp = None
        self.connected = False
        self.path = host_path
        self.__account = {'user': username, 'passwd': password}
        self.host = host
        self.__connect()

    def __connect(self):
        """
        Connect to FTP server
        """
        self.connected = False
        i = 1
        while i <= 3:
            try:
                self.__ftp = ftplib.FTP(self.host, timeout=TIMEOUT)
                # self.__ftp.set_debuglevel(1) # DEBUG MODE
                self.connected = True
                break
            except socket.error as e:
                logger.warning('Cannot reach %s (%i/3) - %s', self.host, i, e)
                time.sleep(i/10.)
                i += 1
                continue

        if not self.connected:
            logger.error('FTP connection to %s => Fail', self.host)
            return
        logger.debug('FTP Connection to %s', self.host)

        self.__ftp.set_pasv(False)  # mode actif

        try:
            self.__ftp.login(user=self.__account['user'],
                             passwd=self.__account['passwd'])
        except ftplib.error_perm:
            logger.error('login %s rejected', self.__account['user'])
            self.quit()
        #self.__ftp.set_pasv(False)  # mode actif
        logger.info('Connected to %s logged as %s', self.host,
                    self.__account['user'])

        if self.path != '':
            self.cd()

    def __cd(self, ftp_path):
        """
        Change to "ftp_path" directory
        """
        if not self.connected:
            self.__connect()

        time.sleep(WAIT_TIME)
        logger.debug('Trying to go to %s', ftp_path)
        try:
            self.__ftp.cwd(ftp_path)
        except socket.error as e:
            logger.error('Cannot reach (%s) : %s', self.host, e)
            self.quit()
            return False
        except ftplib.error_perm:
            logger.error('cannot go to %s', ftp_path)
            self.quit()
            return False

        if not self.connected:
            logger.warning('perte de connexion avec FTP')
            return False

        logger.debug('GO TO %s', ftp_path)
        return True

    def nlst(self):
        """
        Return list of files
        """
        lstf = None
        time.sleep(WAIT_TIME)
        logger.debug('Trying to get list of files...')
        try:
            lstf = self.__ftp.nlst()
        except socket.error as e:
            logger.warning('Cannot get list of files in %s => %s',
                           self.host, e)
        else:
            logger.debug('FTP list : %s', ', '.join(lstf))
        return lstf

    def cd(self, ftp_path=None, recursive=True):
        """
        Change the directory on the server, if need be.
        If recursive is true, directories are created if necessary to get to the
        full path.
        Returns true if the directory is changed.
        """
        if ftp_path is None:
            ftp_path = self.path

        # get folders in path
        folders = []
        while 1:
            ftp_path, folder = os.path.split(ftp_path)
            if folder != "":
                folders.append(folder)
            else:
                if ftp_path != "":
                    folders.append(ftp_path)
                break
        folders.reverse()

        # go recursively to 'ftp_path'
        for folder in folders:
            lstf = self.nlst()
            if lstf is None:
                self.quit()
                return False
            if recursive and not folder in lstf:
                time.sleep(WAIT_TIME)
                self.__ftp.mkd(folder)
                logger.debug('Folder %s created', folder)
            if not self.__cd(folder):
                self.quit()
                return False

        if self.connected:
            time.sleep(WAIT_TIME)
            self.path = self.__ftp.pwd()
        else:
            self.path = None
        return True

    def put(self, local_path, ftp_path, mode='ascii'):
        """
        Put a file to the server, in binary/ascii mode
        """
        if not self.connected:
            return False
        ftp_dir, ftp_file = os.path.split(ftp_path)
        self.cd(ftp_dir)
        if mode == 'ascii':
            file_ = open(local_path, "r")
            self.__ftp.storlines("STOR %s" % ftp_file, file_)
        elif mode == 'binary':
            file_ = open(local_path, "rb")
            self.__ftp.storbinary("STOR %s" % ftp_file, file_)
        logger.debug("%s stored as %s", ftp_path, mode)
        return True

    def delete(self, ftp_path):
        """
        Delete a file on the server.
        """
        if not self.connected:
            return False
        ftp_dir, ftp_file = os.path.split(ftp_path)
        if self.cd(ftp_dir, 0):
            try:
                self.__ftp.delete(ftp_file)
                logger.debug("%s deleted", ftp_path)
            except ftplib.error_perm:
                logger.warning('Unable to delete %s', ftp_path)
                return False
        return True

    def quit(self):
        """
        Quit.
        """
        logger.debug('trying to quit...')
        self.connected = False
        if not self.__ftp is None:
            try:
                self.__ftp.quit()
            except:
                logger.error('Unable to quit properly')
        self.__ftp = None

class FtpUpload(object):
    """
    Provides intelligent FTP uploading of files, using MD5 hashes to track
    which files have to be uploaded.  Each upload is recorded in a local
    file so that the next upload can skip the file if its contents haven't
    changed.  File timestamps are ignored, allowing regenerated files to
    be properly uploaded only if their contents have changed.

    Call `connect` and `set_md5` to establish the settings for a session,
    then `upload` for each set of files to upload.  If you want to have
    removed local files automatically delete the remote files, call
    `deleteOldFiles` once, then `finish` to perform the closing bookkeeping.

    ::

        fu = FtpUpload()
        fu.connect('ftp.myhost.com', 'myusername', 'password')
        fu.set_md5('myhost.md5')
        fu.upload(
            hostdir='www', src='.',
            ascii='*.html *.css', binary='*.gif *.jpg'
        )
        # more upload() calls can go here..
        fu.deleteOldFiles()
        fu.finish()

    """

    def __init__(self, host, username, password, host_path=''):
        self.ezftp = EzFtp(host, username, password, host_path)
        # md5 signatures
        self.md5 = dict()
        self.md5['file'] = None
        self.md5['local'] = {}  # of local files (dictionnary)
        self.md5['remote'] = {}  # of remote files (dictionnary)
        self.md5['upload'] = {}  # of uploaded files (to track them)

    def set_md5(self, md5file):
        """
        Assign a filename to use for the MD5 tracking.
        """
        self.md5['file'] = md5file
        if self.md5['file']:
            try:
                inf = open(self.md5['file'], "r")
                self.md5['local'] = pickle.load(inf)
                self.md5['upload'].update(self.md5['local'])
                inf.close()
            except IOError:
                logger.debug('Fichier md5 inexistant (%s)', self.md5['file'])
                self.md5['local'] = {}
            except EOFError:
                logger.warning('Fichier md5 (%s) vide ou corrompu',
                               self.md5['file'])
                self.md5['local'] = {}

    def upload(self, hostdir='.', ascii=None, binary=None, src='.'):
        """
        Upload a set of files.
        Source files are found in the directory named by `src`
        (and its subdirectories recursively).  The files are uploaded
        to the directory named by `hostdir` on the remote host.
        Files that match one of the space-separated patterns in `ascii`
        are uploaded as text files, those that match the patterns in
        `binary` are uploaded as binary files.

        This method can be called a number of times to upload different
        sets of files to or from different directories within the same
        FtpUpload session.
        """

#        if not self.ezftp:
#            if not self.__ftp:
#                self.__ftp = Tracer('ftp', sys.stdout)
#            self.ezftp = EzFtp(self.__ftp)

        if hostdir != '.' and hostdir != self.ezftp.path:
            self.ezftp.cd(hostdir)

        # dct_path is a dict of ftp mode to upload local files
        dct_path = {}
        if ascii is not None:
            for pat in ascii.split():
                dct_path[pat] = 'ascii'
        if binary is not None:
            for pat in binary.split():
                dct_path[pat] = 'binary'

        # Walk the tree, putting files to the ezftp.
        src_path = path.Path(src)
        for cur_path in src_path.walkfiles():
            rel_path = src_path.relpathto(cur_path)  # relative path
            str_rel_path = str(rel_path)

            # Compute this file's MD5 fingerprint
            actual_md5 = md5.new()
            file_ = open(cur_path, "rb")
            for line in file_.readlines():
                actual_md5.update(line)
            actual_md5 = actual_md5.hexdigest()
            # What was the last MD5 fingerprint?
            previous_md5 = self.md5['local'].get(str_rel_path, '')

            # If the current file is different, then put it to the server.
            if not previous_md5 is None and actual_md5 != previous_md5:
                # Find the mode (binary/ascii), and use the ftp function
                # from the map.
                for pat in dct_path.keys():
                    if cur_path.fnmatch(pat):
                        self.ezftp.put(cur_path, rel_path, mode=dct_path[pat])
            else:
                logger.debug('%s unchanged', str_rel_path)

            # Remember the new fingerprint.
            self.md5['remote'][str_rel_path] = actual_md5
            self.md5['upload'][str_rel_path] = actual_md5

    def delete_remote_old(self):
        """
        Delete any remote files that we have uploaded previously but
        that weren't considered in this FtpUpload session.  This doesn't
        touch files that exist on the remote host but were never uploaded
        by this module.
        """

        # Files in md5['local'] but not in md5['remote'] must have been removed
        for file_ in self.md5['local']:
            if file_ not in self.md5['remote']:
                self.ezftp.delete(file_)  # delete file on FTP server
                del self.md5['upload'][file_]

    def finish(self):
        """
        Do our final bookkeeping.
        """
        # Done with ftp'ing.
        self.ezftp.quit()

        # Write the md5 control file out for next time.
        if self.md5['file']:
            outf = open(self.md5['file'], "w")
            pickle.dump(self.md5['upload'], outf)
            outf.close()

        return True
