#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Configuration de raspberry Weather
Created on Thu Mar  6 15:21:35 2014

@author: thomas_a
"""
import os
import ConfigParser
import logging

logger = logging.getLogger("rpi_log."+__name__)


class config(object):
    """
    Charge et verifie la configuration de l'application à partir d'un fichier
    de configuration (file_config)
    """
    station = dict()
    dirs = dict()
    web = dict()
    os = dict()

    def __init__(self, file_config):
        if not os.path.exists(file_config):
            str_msg = "Fichier de configuration (%s) absent" % file_config
            logger.critical(str_msg)
            raise ConfigParser.Error('Erreur Configuration Raspberry')
        self._file_cfg = file_config
        self.read()

    def check(self):
        """
        Verification que les paramètres sont OK
        """
        nb_pbs = 0
        if not os.path.exists(self.dirs['bin']):
            logger.critical("Le dossier des executables (%s) n'existe pas.",
                            self.dirs['bin'])
            nb_pbs += 1
        if not os.path.exists(self.dirs['out']):
            os.makedirs(self.dirs['out'])
        try:
            self.station['frequence'] = int(self.station['frequence'])
        except ValueError:
            logger.critical("La fréquence d'archivage doit être un nombre")
            nb_pbs += 1
        if self.station['frequence'] is not None:
            if self.station['frequence'] not in [1, 5, 10, 15, 30, 60, 120]:
                logger.critical("Fréquence d'archivage incompatible avec les \
                             stations")
                nb_pbs += 1
        else:
            self.station['frequence'] = 60

        if self.station['id'] is None or self.station['id'] == '':
            logger.critical("Pas d'identifiant de station")
            nb_pbs += 1

        if self.station['model'] not in ['VantagePro2', 'WeatherMII']:
            logger.critical("Modèle de station inconnue")
            nb_pbs += 1

        if self.web['connection'] not in ["wifi", "filaire"]:
            logger.critical("Mode de connection a internet inconnu : %s",
                            self.web['connection'])
            nb_pbs += 1
        if self.web['connection'] == "wifi" and self.web['ssid'] == '':
            logger.critical("Aucun réseau wifi enregistre")
            nb_pbs += 1
        return nb_pbs

    def read(self):
        """
        Recupere la configuration de la station meteo telle qu'enregistree dans
        le fichier de configuration
        """
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(self._file_cfg)

        lst = ['id', 'model', 'frequence']
        for param in lst:
            self.station[param] = cfgparser.get('station', param).strip()

        lst = ['connection', 'url', 'serveur', 'host', 'pw', 'login', 'ssid',
               'http_proxy', 'ftp_proxy', 'ftp_path']
        for param in lst:
            self.web[param] = cfgparser.get('web', param).strip()
        for param in ['connection', 'url', 'serveur', 'host', 'ssid']:
            self.web[param] = self.web[param].lower()

        self.dirs['bin'] = os.path.normpath(cfgparser.get('os', 'dirBin'))
        self.dirs['out'] = os.path.normpath(cfgparser.get('os', 'dirOut'))
        self.os['pw'] = cfgparser.get('os', 'pw')
        self.os['pid'] = cfgparser.get('os', 'pid')
        self.os['watchlog'] = cfgparser.get('os', 'watchlog')
        cfgparser = None

        nb_pbs = self.check()
        if nb_pbs == 0:
            logger.info('Demarrage RPI Weather...')
            logger.info('Station : %s (%s)', self.station['id'],
                        self.station['model'])
        else:
            logger.critical('%i Problemes detecte(s)', nb_pbs)
            raise ConfigParser.Error('Erreur Configuration Raspberry')
        return nb_pbs
