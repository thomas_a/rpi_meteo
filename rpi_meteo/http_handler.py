#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Handler pour HTTP
Created on Wed Dec 31 18:53:15 2014

@author: thomas_a
"""

import requests
import socket
import os

import logging

log = logging.getLogger("rpi_log."+__name__)


class HTTP_Handler(object):
    """
    Handler pour HTTP
    """
    _TIME_OUT = .5
    _RETRIES = 3

    def __init__(self, proxy):
        # definition du proxy (si defini)
        self.proxy = {'http':proxy['http_proxy'], 
                      'https':proxy['http_proxy'],
                      'ftp':proxy['ftp_proxy']}
        os.environ['HTTP_PROXY'] = proxy['http_proxy']
        os.environ['HTTPS_PROXY'] = proxy['http_proxy']
        os.environ['FTP_PROXY'] = proxy['ftp_proxy']
        
        if self.proxy['http'] == '' and self.proxy['ftp'] == '':
            log.debug('Pas de proxy')
        else:
            log.debug('PROXY - http/https=%s | ftp=%s', self.proxy['http'],
                      self.proxy['ftp'])

    def get_request(self, url, fields=None):
        """
        Emet une requete et renvoie l'heure du serveur
        """
        r = None
        e = None
        i = 0
        while i < self._RETRIES:
            log.debug('Requesting %s (%i/%i)', url, i+1, self._RETRIES)
            try:
                r = requests.get(url, params=fields, proxies=self.proxy,
                                 timeout=self._TIME_OUT,
                                 verify=False)
                #r = session.get(url, params=fields, proxies=self.proxy,
                #                 timeout=self._TIME_OUT)
            except requests.exceptions.ProxyError as e:
                log.warning("PROXY ERROR (%s, %i/3)", self.proxy['http'],
                            i + 1)
                log.debug(e)
            except socket.timeout as e:
                log.warning("Socket timed out (%i/3)", i + 1)
            except requests.exceptions.Timeout as e:
                log.warning("Web TIMEOUT with %s (%i/3)", url, i + 1)
            except requests.exceptions.ConnectionError as e:
                log.warning('Web ConnectionError with %s (%i/3)', url, i + 1)
            except requests.exceptions.InvalidURL as e:
                log.warning('%s give invalid url (%i/3)', url, i + 1)

            if r is None:
                log.debug(e)
                log.warning('GET request failed (%i/3)', i + 1)
            else:
                if r.status_code == 200:
                    break
                else:
                    log.warning('Request => %i HTTP Error (%i/3)',
                                r.status_code, i + 1)
            i += 1

        if r is None:
            log.error('No response to GET request (%s)', url)
            return None
        log.debug('URL %s requested', r.url)
        if r.status_code == 200:
            log.info('HTTP request (%s) => OK', r.headers['Date'])
        else:
            log.error('HTTP request (%s) => Error %s', r.headers['Date'],
                        r.status_code)
            return None
        s_date = r.headers['date']

        r.close()
        return s_date

    def check_url(self, url):
        """
        Verifie la connexion internet en testant une url
        """
        try:
            requests.head(url, proxies=self.proxy, timeout=self._TIME_OUT)
            return True
        except requests.exceptions.ProxyError:
            log.warning('PROXYERROR (%s)', self.proxy['http'])
        except requests.exceptions.Timeout:
            log.warning('TIMEOUT with %s', url)
        except requests.exceptions.ConnectionError:
            log.warning('Web ConnectionError with %s', url)
        return False

    def __del__(self):
        pass
