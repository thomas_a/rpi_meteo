#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Sauvegarde sur FTP :
- des données météo collectées
- des fichiers logs
Created on Mon Feb 16 11:47:42 2015

@author: thomas_a
"""
import os
from datetime import datetime, timedelta

import logging
import sys

from rpi_meteo import cfg
from rpi_meteo.internet import Internet
from rpi_meteo.ftp_upload import FtpUpload

log = logging.getLogger('rpi_log.backup')


def clean_local_files(local_path, age=5):
    """
    Delete old local files that we have uploaded a few days ago (> "age" in
    days)
    """
    if not os.path.exists(local_path):
        log.error('Wrong path to clean : %s', local_path)
        return None

    if age < 0:
        log.error('age of local files should be > 0')
        return None

    age = timedelta(days=age)
    lst_files = [f for f in os.listdir(local_path) if f[-4:] == '.csv']
    log.info('Cleaning old data files (%i data files)...', len(lst_files))

    nb_keep = nb_del = 0
    for f in lst_files:
        date_f = f.split('_')[-1]  # get files' date
        try:
            date_f = datetime.strptime(date_f.split('.')[0], '%Y-%m-%d')
        except ValueError:
            continue
        # autre solution avec date du fichier
        #date_f = datetime.fromtimestamp(os.path.getctime(tmp))
        age_file = datetime.today() - date_f
        if age_file < timedelta(days=0):
            log.warning('Date du fichier %s', f)
        elif age_file <= age:
            nb_keep += 1
            log.debug('%s => a conserver', f)
        elif age_file > age:
            nb_del += 1
            os.remove(os.path.join(local_path, f))
            log.debug('%s deleted', f)

    log.info('Clean data files => %i kept - %i deleted', nb_keep, nb_del)

if __name__ == '__main__':
    log.info('Backup going to start...')
    PROXIES = {"http_proxy": cfg.web['http_proxy'],
               "ftp_proxy": cfg.web['ftp_proxy']}
    web = Internet(cfg.web['connection'], cfg.web['url'],
                   PROXIES, cfg.web['ssid'])
    if not web.connected:
        log.error('No web connection - Unable to backup')
        sys.exit(1)
    else:
        log.debug('Web OK. Backup...')

    FTP_PATH = os.path.join(cfg.web['ftp_path'], cfg.station['id'])
    fu = FtpUpload(cfg.web['host'], cfg.web['login'], cfg.web['pw'], FTP_PATH)
    if not fu.ezftp.connected:
        log.error('FTP connection aborted => backup fails')
    else:
        log.debug('FTP connection OK')
        fu.set_md5(os.path.join(cfg.dirs['bin'], '.rpi_weather.md5'))
        fu.upload(src=cfg.dirs['out'], ascii='*.csv *.log*')
        fu.finish()
        # suppression des fichiers locaux deja sauvegardes et anciens
        # TODO: désactivé car efface fichiers même si ils ne sont pas 
        # sauvegardés
        #clean_local_files(cfg.dirs['out'], age=30)
        log.info('Backup finished')
