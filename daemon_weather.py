#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Collecte de données météorologiques : daemon
Created on Wed Dec 31 18:42:05 2014

@author: thomas_a
"""

import time
from datetime import timedelta
import os
import sys
import subprocess

import logging
import logging.config

#from daemon import runner
#from daemonize import Daemonize

from rpi_meteo import cfg
from rpi_meteo.releve import Releve
from rpi_meteo.station import Station
from rpi_meteo.internet import Internet
from rpi_meteo.computer import Computer
from rpi_meteo.daemon import Daemon


class MyDaemon(Daemon):
    """
    Chaine de collecte de données météorologiques à partir de station de la
    marque DAVIS.
    Les données sont collectées à partir de la console et transmises en quasi-
    temps réel sur une page. Quotidiennement, les données archivées sont
    synchronisées avec un serveur FTP
    """
    def __init__(self, pid):
        Daemon.__init__(self, pid)
        self._log = None
        # Composants de la chaine de collecte de donnees
        self._station = None
        self._http = None
        self._pc = None
        self._releve = None

    def wait_pause(self):
        """
        Met en pause avant prochaine acquisition
        """
        # attente avant prochaine acquisition meteo
        # ajout de 3 secondes pour laisser le temps à la console d'enregistrer
        pause = DELAY - (time.time() % DELAY) + 3
        self._log.info('...pause (%.1f s.)...', pause)
        time.sleep(pause)

    def _check_time(self):
        """
        Vérification de l'heure des différentes composantes :
        - station météo
        - raspberry
        - Internet (référence si ntp n'est pas fonctionnel)
        """
        self._log.info('Vérification heure des composants...')
        pc_date = self._pc.date
        self._http.check()
        if not self._http.connected and not self._pc.connected:
            self._log.warning("Unable to get time to set station clock")
            return False

        # verifier si date Raspberry est OK par rapport à date Internet
        if self._http.connected:
            http_date = self._http.date
            if abs(http_date - pc_date) > TIME_TOL:
                self._pc.date = http_date
                self._log.debug("PC clock set from internet")

        # Check station time and update it if necessary
        if not self._station.connected:
            self._log.warning("No station connected - unable to set time")
            return False
	else:
            set_date = None  # date à affecter à la station
            station_date = self._station.date  # date actuelle de la statio
            if station_date is None:
                self._log.warning("Issue with station clock")
                return False

            # heure de reference : d'abord site internet, PC sinon
            if self._http.connected:
                if abs(station_date - http_date) > TIME_TOL:
                    set_date = http_date
            elif self._pc.connected:
                if abs(station_date - self._pc.date) > TIME_TOL:
                    set_date = self._pc.date

            if set_date is None:
                self._log.debug('Station already on time')
            else:
                self._log.debug('Station time needs to be updated')
                self._station.date = set_date

        self._log.info('Heure des composants => OK')
        return True


    def run(self):
        """
        Actions à exécuter sous forme d'un daemon
        """
        # creation du log
        logging.config.fileConfig(os.path.join(cfg.dirs['bin'], 'logging.cfg'))
        self._log = logging.getLogger('rpi_log.daemon_weather')

        # creation des composants de la chaine de collecte de donnees
        self._station = Station(cfg.station['model'],
                                cfg.station['frequence'])
        self._http = Internet(cfg.web['connection'], cfg.web['url'],
                              PROXIES, cfg.web['ssid'])
        self._pc = Computer()

        self._releve = Releve(cfg.station['id'], self._station.model,
                              cfg.dirs['out'])
        # Sauvegarde sur FTP
        self._log.info('Sauvegarde des données sur FTP...')
        script_backup = os.path.join(cfg.dirs['bin'], 'backup_weather.py')
        subprocess.call("sudo python %s start" % script_backup, shell=True)
        # verification que les composants sont tous à l'heure
        self._check_time()
        i = 0
        pbs = {'station': 0, 'internet': 0}  # compteur des pbs
        while True:
            self.wait_pause()
            self._log.debug('Acquisition (%i)', i)

            # COLLECTE des donnees meteo
            data = self._station.parse()
            if len(data) <= 0:
                pbs['station'] += 1
            else:
                pbs['station'] = 0

            data['utc_date_PC'] = self._pc.str_date  # ajout date du PC
            self._log.debug('Données station => Releve...')
            self._releve.fields = data  # affectation du releve
            self._log.info('Releve créé')

            # PUBLICATION des donnees
            self._log.debug('Publication (%i)', i)
            self._releve.to_file()  # a) dans un fichier
            # b) sur le web (emission requete GET)
            self._http.request(cfg.web['url'], fields=self._releve.rtc)
            if self._http.connected:
                self._log.debug('Published on web')
                pbs['internet'] = 0
            else:
                pbs['internet'] += 1
            self._log.info('publish (%s)', self._releve.rtc['date'])

            # verification du nbre de pbs :
            if max(pbs.values()) >= 10:
                npbs = max(pbs.values())
                src_pb = [k for k in pbs if pbs[k] == npbs][0]
                self._log.warning('Too many %s issues (%i) => REBOOT...',
                                  src_pb, npbs)
                subprocess.call('reboot', shell=True)
            else:
                self._log.info('Issues during run %i : station (%i), web(%i)',
                               pbs['station'], pbs['internet'])

            # everyday : time is checked + backup
            if i >= N_PAS:
                self._check_time()
                subprocess.Popen("python %s start" % script_backup, shell=True)
                if os.path.exists(WATCHLOG):
                    os.remove(WATCHLOG)
                i = 0
            else:
                i += 1

if __name__ == '__main__':
    # delai entre recolte donnees (sec.)
    DELAY = cfg.station['frequence'] * 60
    N_PAS = (24 * 60) // cfg.station['frequence']
    PROXIES = {"http_proxy": cfg.web['http_proxy'],
               "ftp_proxy": cfg.web['ftp_proxy']}
    TIME_TOL = timedelta(minutes=5)  # Tolerance avant de changer l'heure
    WATCHLOG = os.path.join(cfg.dirs['bin'], cfg.os['watchlog'])

    daemon_weather = MyDaemon(cfg.os['pid'])
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon_weather.start()
        elif 'stop' == sys.argv[1]:
            daemon_weather.stop()
        elif 'restart' == sys.argv[1]:
            daemon_weather.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
