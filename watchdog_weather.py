#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Collecte de données météorologiques : watchdog
Created on Thu Feb 12 09:17:51 2015

@author: thomas_a
"""
import time
import os
import errno
import subprocess
from subprocess import check_output
import logging
import logging.config

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from rpi_meteo import cfg

N_TIME = 10  # number of consecutive times daemon should be restarted
FREQ = 30 # fréquence de surveillance par le watchdog (en secondes)


def check_pidfile(pid_file):
    """
    Check if pid file exists and return pid number inside (-1 else)
    """
    pid_number = -1
    try:
        with open(pid_file, 'r') as file_:
            pid_number = int(file_.read().strip())
    except ValueError:
        logger.error('Daemon : wrong pid')
        try:
            os.remove(pid_file)
        except OSError:
            logger.error('Unable to delete pid file')
        else:
            logger.info('PID file deleted (%s)', pid_file)
    except IOError:
        logger.error('Daemon : pid file is missing (%s)', pid_file)

    return pid_number

def pid_exists(pid):
    """Check whether pid exists in the current process table."""
    if pid < 0:
        return False
    try:
        os.kill(pid, 0)
    except OSError as e:
        return e.errno == errno.EPERM
    else:
        return True


class MyHandler(FileSystemEventHandler):
    """
    Watchdog associé à des évènements liés aux fichiers pid utilisé par le
    daemon
    """
    # count how many times daemon has to be restarted
    _restart = [0, time.time()]

    def on_deleted(self, event):
        fname = os.path.basename(event.src_path)
        if fname == os.path.basename(cfg.os['pid']):
            # if last restart occurs recently (< 2hrs)
            if abs(time.time() - self._restart[1]) < 7200:
                self._restart = [self._restart[0]+1, time.time()]
            else:
                self._restart = [0, time.time()]
            if self._restart[0] >= N_TIME:
                logger.error('Daemon relancé %i fois => REBOOT',
                             self._restart[0])
                subprocess.call('reboot', shell=True)
            else:
                logger.warning('Daemon arrete (%i/%i)', self._restart[0],
                               N_TIME)
                time.sleep(2)
                try:
                    response = check_output(["service", "daemon_weather",
                                             "start"])
                except subprocess.CalledProcessError:
                    logger.critical("Daemon service missing => reinstall")
                else:
                    if response == 0:
                        logger.info('Daemon restarted (%i/%i)',
                                    self._restart[0], N_TIME)
                    else:
                        logger.warning('Issue to restart Daemon')

if __name__ == "__main__":
    # creation du log
    LOG_FILENAME = os.path.join(cfg.dirs['out'], 'rpi_watch.log')
    logger = logging.getLogger('rpiwatch')
    logger.setLevel(logging.INFO)
    # Add the log message handler to the logger
    handler = logging.handlers.RotatingFileHandler( \
              LOG_FILENAME, maxBytes=2000, backupCount=5)
    # create formatter and add it to the handlers
    formatter = '%(asctime)s || %(name)s || %(levelname)s || %(message)s'
    formatter = logging.Formatter(formatter)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # creation du watchdog
    evt_handler = MyHandler()
    observer = Observer()
    observer.schedule(evt_handler, path=os.path.dirname(cfg.os['pid']),
                      recursive=False)
    observer.start()
    logger.info('Wathchdog démarré')

    # 1ère vérification
    pid_daemon = check_pidfile(cfg.os['pid'])
    if pid_daemon == -1:
        try:
            check_output(["service", "daemon_weather", "start"])
            logger.info('Lancement du Daemon...')
        except subprocess.CalledProcessError:
            logger.critical("Daemon manquant => réinstaller application")

    try:
        i = 0
        while True:
            # vérif que le pid est OK, sinon on efface le fichier PID
            # => le watchdog se chargera de redémarrer le daemon
            pid_daemon = check_pidfile(cfg.os['pid'])

            if not pid_exists(pid_daemon):
                logger.error('Daemon is dead')

            if i >= 240:  # watchdog se manifeste toutes les 20 minutes
                logger.info("Watchdog alive")
                i = 0
            else:
                i += 1
            time.sleep(FREQ)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
