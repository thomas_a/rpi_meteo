# README #

** RPI meteo ** est une application (codée en Python) permettant la récolte de données météorologiques à partir de stations Davis (Weather Monitor II ou Vantage Pro 2). Cette application se destine au Raspberry PI mais peut être porté sur tout système Linux (au moins ceux basés sur Debian).

Les données météorologiques sont affichées en temps quasi-réel sur un site Web et archivées quotidiennement sur un serveur FTP. 

** RPI meteo ** est une application libre, sous licence lGPL (reportez-vous au fichier LICENSE, pour de plus amples informations).

### Comment installer rpi_meteo ? ###
* Télécharger le code :

```
#!sh

git clone https://thomas_a@bitbucket.org/thomas_a/rpi_meteo.git
```
* Configurer l'application : copier le fichier de configuration 'rip_meteoORIGINAL.cfg' et renommer le 'rip_meteo.cfg'. Modifier le contenu (modèle de station, configuration internet...).
* Installer l'application grâce au script dédié (en utilisateur root)
```
#!sh

python ~/rpi_weather/setup.py

```
L'application est prête à fonctionner ! Vous pouvez :
* démarrer l'application (en tant que root) :

```
#!sh

service daemon_weather start

```
* tester que l'application fonctionne bien :
```
#!sh

python ~/rpi_weather/test.py

```
* redémarrer le Raspberry pi et l'application se mettra automatiquement en marche

### Les composants de RPI meteo ###
L'application se destine au Raspberry Pi. Le système doit être connecté à une station météorologique (à l'aide d'un port USB ou série) et à Internet (réseau filaire ou wifi). 

+ Stations météorologiques : seules les stations Davis Weather Monitor II et Vantage Pro 2 sont actuellement supportées
+ Dépendances
    * Librairies Python : cf. fichier "requirements.txt"
    * Logiciel : Python 2.7+, python-pip, ntpdate, libyaml-dev

### Besoin de plus d'informations ? ###

Rendez-vous sur le Wiki ?

### Contacts ###

* Hervé QUENOL (herve.quenol@univ-rennes2.fr)
* Alban THOMAS (alban.thomas@univ-rennes2.fr)