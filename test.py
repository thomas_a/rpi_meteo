# -*- coding: utf-8 -*-
"""
Created on Mon Feb 16 11:47:42 2015

@author: thomas_a
"""
from __future__ import print_function
import os
import ConfigParser
from datetime import datetime
import unittest
import email.utils as eut
import time

from rpi_meteo.config import config
from rpi_meteo.releve import Releve
from rpi_meteo.base import Base
from rpi_meteo.station import Station, UnknownStationException
from rpi_meteo.internet import Internet
from rpi_meteo.http_handler import HTTP_Handler
from rpi_meteo.computer import Computer
from rpi_meteo.wmonitor2 import WeatherMII
from rpi_meteo.vantage_pro2 import VantagePro
from rpi_meteo.ftp_upload import EzFtp, FtpUpload
from rpi_meteo import cfg
from backup_weather import clean_local_files

PROXIES = {"http_proxy": cfg.web['http_proxy'],
           "ftp_proxy": cfg.web['ftp_proxy']}
CFG_FILE = 'rpi_meteo.cfg'


class TestCleanLocal(unittest.TestCase):
    """
    Tests unitaires de suppression des anciens fichiers sur le PC
    (après sauvegarde sur FTP)
    """
    def test_wrong_local_path(self):
        """
        Effacement du contenu d'un dossier inexistant
        """
        self.assertIsNone(clean_local_files('/path/inexistant'))

    def test_path_wrong_age(self):
        """
        Effacement de fichiers en fonction d'un age erroné
        """
        self.assertIsNone(clean_local_files(cfg.dirs['out'], -5))

    def test_local_path(self):
        """
        Vérifie que les fichiers méritant d'être supprimés le sont bien
        """
        n_files_before = len(os.listdir(cfg.dirs['out']))
        clean_local_files(cfg.dirs['out'])
        self.assertLessEqual(len(os.listdir(cfg.dirs['out'])), n_files_before)

class TestFtp(unittest.TestCase):
    """
    Tests unitaires de communication par FTP
    """
    def test_ezftp_upload(self):
        """
        Envoi d'un fichier sur le serveur FTP
        """
        ftp = EzFtp(cfg.web['host'], cfg.web['login'],
                    cfg.web['pw'], cfg.web['ftp_path'])
        retour = ftp.put('logging.cfg', 'logging.cfg')
        self.assertTrue(retour)

    def test_ftp_backup(self):
        """
        Test d'envoi de tous les fichiers nouveaux sur FTP
        """
        ftp_path = os.path.join(cfg.web['ftp_path'], cfg.station['id'])
        ftp_up = FtpUpload(cfg.web['host'], cfg.web['login'], cfg.web['pw'],
                           ftp_path)
        ftp_up.set_md5('.rpi_weather.md5')
        ftp_up.upload(src=cfg.dirs['out'], ascii='*.csv *.log*')
        self.assertTrue(ftp_up.finish())

class TestStation(unittest.TestCase):
    """
    Tests unitaires sur la station météorologique
    """
    @classmethod
    def setUpClass(cls):
        cls.__station = Station(cfg.station['model'], 1)
        cls.__pc = Computer()

    def test_check(self):
        """
        Test de vérification de la configuration de la station
        """
        if not self.__station.connected:
            self.fail('Station not connected')

        # check() est appelle dans init
        self.assertTrue(self.__station.connected)

    def test_wrong_model(self):
        """
        Test de création d'une station d'un modèle inconnu
        """
        self.assertRaises(UnknownStationException, Station, 'nimportequoi', 1)

    def test_get_date_type(self):
        """
        Vérification du type de date renvoyé par la station
        """
        if not self.__station.connected:
            self.fail('Station not connected')
        self.assertEqual(type(self.__station.date), datetime)

    def test_set_date(self):
        """
        Test de changement de date de la station
        """
        if not self.__station.connected:
            self.fail('Station not connected')
        actual_date = self.__pc.date

        test_date = datetime(1998, 7, 14, 23, 10)
        self.__station.date = test_date
        set_date = self.__station.date
        self.__station.date = actual_date
        self.assertEqual(set_date.timetuple()[1:5],
                         test_date.timetuple()[1:5])

    def test_set_wrong_date(self):
        """
        Test d'affectation d'une date erronée (ex : nombre 2012) à la station
        """
        if not self.__station.connected:
            self.fail('Station not connected')
        self.assertRaises(TypeError, self.__station.date, 2012)

    def test_parse(self):
        """
        Test de récupération des données météorologiques par la station
        """
        if not self.__station.connected:
            self.fail('Station not connected')
        data = self.__station.parse()
        self.assertGreater(len(data), 10)

    def test_erase_memory(self):
        """
        Vérification que le changement d'intervalle d'archivage de la station
        implique bien un effacement de la mémoire ('NextRec' == 0)
        """
        if not self.__station.connected:
            self.fail('Station not connected')
        self.__station = None
        self.__station = Station(cfg.station['model'], 10)
        data = self.__station.parse()
        self.assertEqual(data['NextRec'], 0)

class TestConsole(unittest.TestCase):
    """
    Tests unitaires sur la console météorologique
    """
    @classmethod
    def setUpClass(cls):
        cls.__port = None
        station = Station(cfg.station['model'], 1)
        if station.connected:
            cls.__port = station.port
        else:
            return None

        if cfg.station['model'] == 'WeatherMII':
            cls.__console = WeatherMII(device=cls.__port, log_interval=1)
        elif cfg.station['model'] == 'VantagePro2':
            cls.__console = VantagePro(device=cls.__port, log_interval=1)
        station = None

    def test_wrong_port(self):
        """
        Test connexion console sur un mauvais port
        """
        str_port = '/nimporte/quoi'
        freq = 1
        if cfg.station['model'] == 'WeatherMII':
            console = WeatherMII(device=str_port, log_interval=freq)
        elif cfg.station['model'] == 'VantagePro2':
            console = VantagePro(device=str_port, log_interval=freq)
        self.assertIsNone(console.port)

    def test_get_date(self):
        """
        Vérification du type de date renvoyé par la console
        """
        if self.__port is None:
            self.fail('Pas de console disponible')
        temps = self.__console.get_time()
        self.assertEqual(type(temps), time.struct_time)

    def test_set_date(self):
        """
        Test de changement de la date de la console
        """
        if self.__port is None:
            self.fail('Pas de console disponible')
        pc = Computer()
        set_date = pc.date.timetuple()
        self.assertEqual(self.__console.set_time(set_date), 0)

    def test_set_arcinterval(self):
        """
        Test de définition de l'intervalle d'archivage de la console
        """
        if self.__port is None:
            self.fail('Pas de console disponible')
        old_interval = self.__console._get_archive_interval()
        if old_interval == 1:
            new_interval = 10
        else:
            new_interval = 1

        self.__console._set_archive_interval(new_interval)

        self.assertEqual(self.__console._get_archive_interval(), new_interval)

class TestComputer(unittest.TestCase):
    """
    Tests unitaires sur le PC (raspberry)
    """
    def setUp(self):
        self.__pc = Computer()

    def test_get_date(self):
        """
        Test de récupération de la date système
        """
        self.assertEqual(self.__pc.date.timetuple(),
                         datetime.utcnow().timetuple())

    def test_get_str_date(self):
        """
        Test de récupération de la date système, sous forme d'un string
        """
        _date = datetime.utcnow()
        str_date = datetime.strftime(_date, "%Y-%m-%d %H:%M:%S")
        self.assertEqual(self.__pc.str_date, str_date)

    def test_set_date(self):
        """
        Test d'affectation de la date système
        """
        actual_date = self.__pc.date
        test_date = datetime(1998, 7, 14, 23, 10)
        self.__pc.date = test_date
        set_date = self.__pc.date
        self.__pc.date = actual_date
        self.assertEqual(set_date.timetuple()[:5], test_date.timetuple()[:5])

    def test_set_wrong_date(self):
        """
        Test d'affectation de la date système à partir d'une valeur erronée
        """
        self.assertRaises(TypeError, self.__pc.date, 'this is not a date')


class TestBase(unittest.TestCase):
    """
    Test de conversion entre base 10 et hexa
    """
    def setUp(self):
        self.__base = Base()

    def test_base_decoding_upper(self):
        """
        Test de décodage d'un nombre en Hexadécimal (écrit en majuscule)
        """
        self.assertEqual(self.__base.decode('A'), 10)

    def test_base_decoding_lower(self):
        """
        Test de décodahe d'un nombre en Hexadécimal (écrit en minuscule)
        """
        self.assertEqual(self.__base.decode('a'), 10)

    def test_base_encoding(self):
        """
        Test d'encodage d'un nombre en hexadécimal
        """
        self.assertEqual(self.__base.encode(16), 10)

    def test_base_encoding_single_num(self):
        """
        Test d'encodage d'un nombre composé d'un seul chiffre en hexadécimal
        """
        self.assertEqual(self.__base.encode(9), 9)

class TestReleve(unittest.TestCase):
    """
    Tests unitaires sur un relevé météorologique
    """
    @classmethod
    def setUpClass(cls):
        cls.releve = Releve(cfg.station['id'], cfg.station['model'],
                            cfg.dirs['out'])
        station = Station(cfg.station['model'], 1)
        _date = datetime.utcnow()
        str_date = datetime.strftime(_date, "%Y-%m-%d %H:%M:%S")
        cls.__connected = station.connected
        if station.connected:
            data = station.parse()
        else:
            data = {'id':cfg.station['id'], 'arcDateStation':str_date}
        data['utc_date_PC'] = str_date  # ajout date du PC
        cls.releve.fields = data
        cls.__data = cls.releve.fields
        station = None

    def test_tmp_values(self):
        """
        Vérification des valeurs des données météorologiques relevées :
        température
        """
        if not self.__connected:
            self.fail('Station non connectee')
        lst_params = ['TempOut', 'TempIn', 'avgTempOut', 'hiTempOut',
                      'loTempOut', 'avgTempIn']
        test_values = True
        for param in lst_params:
            test_values = test_values and (self.__data[param] is None \
                          or (self.__data[param] > -20 and
                              self.__data[param] < 40))
        if not test_values:
            for param in lst_params:
                print('%s : %s' % (param, str(self.__data[param])))
        self.assertTrue(test_values)

    def test_pp_values(self):
        """
        Vérification des valeurs des données météorologiques relevées :
        précipitation
        """
        if not self.__connected:
            self.fail('Station non connectee')
        lst_params = ['RainRate', 'arcRainRate', 'hiRainRate']
        test_values = True
        for param in lst_params:
            test_values = test_values and (self.__data[param] is None \
                          or (self.__data[param] >= 0 and
                              self.__data[param] < 40))
        self.assertTrue(test_values)

    def test_hum_values(self):
        """
        Vérification des valeurs des données météorologiques relevées :
        humidité
        """
        if not self.__connected:
            self.fail('Station non connectee')
        lst_params = ['HumOut', 'HumIn', 'arcHumOut', 'arcHumIn']
        test_values = True
        for param in lst_params:
            test_values = test_values and (self.__data[param] is None \
                          or (self.__data[param] >= 0 and
                              self.__data[param] <= 100))
        self.assertTrue(test_values)

    def test_wsp_values(self):
        """
        Vérification des valeurs des données météorologiques relevées :
        vitesse du vent
        """
        if not self.__connected:
            self.fail('Station non connectee')
        lst_params = ['WindSpeed', 'WindSpeed10Min', 'avgWind', 'hiWind']
        test_values = True
        for param in lst_params:
            test_values = test_values and (self.__data[param] is None \
                          or (self.__data[param] >= 0 and
                              self.__data[param] < 30))
        self.assertTrue(test_values)

    def test_wdir_values(self):
        """
        Vérification des valeurs des données météorologiques relevées :
        direction du vent
        """
        if not self.__connected:
            self.fail('Station non connectee')
        lst_params = ['WindDir', 'avgWindDir', 'hiWindDir']
        test_values = True
        for param in lst_params:
            test_values = test_values and (self.__data[param] is None \
                          or (self.__data[param] >= 0 and
                              self.__data[param] < 360))
        self.assertTrue(test_values)

    def test_pr_values(self):
        """
        Vérification des valeurs des données météorologiques relevées :
        pression atmosphérique
        """
        if not self.__connected:
            self.fail('Station non connectee')
        lst_params = ['Pressure', 'arcPressure']
        test_values = True
        for param in lst_params:
            test_values = test_values and self.__data[param] >= 950 \
                          and self.__data[param] <= 1075
        self.assertTrue(test_values)

    def test_battery_values(self):
        """
        Vérification des valeurs du relevé :
        Battery status
        """
        if not self.__connected:
            self.fail('Station non connectee')
        param = 'BatteryStatus'
        test = True
        if cfg.station['model'] == 'VantagePro2':
            test = self.__data[param] >= 0 and self.__data[param] <= 100
        self.assertTrue(test)

    def test_empty(self):
        """
        Test de création d'un relevé vierge
        """
        releve = Releve(cfg.station['id'], cfg.station['model'],
                        cfg.dirs['out'])
        test = releve._id == cfg.station['id'] and \
            len(releve.fields) == 32
        if not test:
            print(releve.rtc)
        self.assertTrue(test)

    def test_rtc_empty(self):
        """
        Vérification des données RTC correspondantes à un relevé vierge
        """
        releve = Releve(cfg.station['id'], cfg.station['model'],
                        cfg.dirs['out'])
        test = releve.rtc['id'] == cfg.station['id'] and \
            len(releve.rtc) == 8
        if not test:
            print(releve.rtc)
        self.assertTrue(test)

    def test_missing_param(self):
        """
        Test de création d'un relevé avec des paramètres manquants
        """
        self.assertRaises(TypeError, Releve)

    def test_set_empty(self):
        """
        Test d'affectation d'un relevé vide
        """
        releve = Releve(cfg.station['id'], cfg.station['model'],
                        cfg.dirs['out'])
        releve.fields = {}
        self.assertEqual(releve.rtc['id'], cfg.station['id'])

    def test_set_fields(self):
        """
        Test d'affectation des champs du relevé
        """
        releve = Releve(cfg.station['id'], cfg.station['model'],
                        cfg.dirs['out'])
        date_st = datetime(1998, 7, 14, 6, 6, 6).strftime("%Y-%m-%d %H:%M:%S")
        dct_values = {'RainRate': 12, 'HumOut': 85.7, 'TmpOut': 12.4,
                      'WindDir': 92.5, 'WindSpeed': 0.1, 'Pressure': 1005.2,
                      'arcDateStation': date_st}
        releve.fields = dct_values
        dct_values['id'] = cfg.station['id']
        self.assertEqual(len(releve.rtc), len(dct_values))

    def test_set_date_from_pc(self):
        """
        Test d'affectation de la date du relevé par le PC
        """
        releve = Releve(cfg.station['id'], cfg.station['model'],
                        cfg.dirs['out'])
        pc = Computer()
        date_set = pc.str_date
        releve.fields = {'utc_date_PC': date_set}
        self.assertEqual(releve.rtc['date'], date_set)

    def test_set_date(self):
        """
        Test d'affectation de la date du relevé
        """
        releve = Releve(cfg.station['id'], cfg.station['model'],
                        cfg.dirs['out'])
        date_set = datetime(1998, 7, 14, 6, 6, 6).strftime("%Y-%m-%d %H:%M:%S")
        releve.fields = {'arcDateStation': date_set}
        self.assertEqual(releve.rtc['date'], date_set)

    def test_rtc(self):
        """
        Vérification que les données RTC sont bien toutes renseignées par la
        station
        """
        if not self.__connected:
            self.fail('Station non connectee')
        lst_p = ['id', 'tmp', 'hum', 'pp', 'wsp', 'pr', 'date']
        rtc_na = [k for k in lst_p if self.releve.rtc[k] is None]
        self.assertEqual(len(rtc_na), 0)

    def test_tempminmax(self):
        """
        Vérification que les températures min, max et moyennes sont bien
        cohérentes (min <= avg & max >= avg)
        """
        if not self.__connected:
            self.fail('Station non connectee')
        test = (self.releve.fields['hiTempOut'] >=
                self.releve.fields['avgTempOut'] and
                self.releve.fields['loTempOut'] <=
                self.releve.fields['avgTempOut'])
        if not test:
            print('T°c (avg, min, max) : %i | %i | %i' %
                  (self.releve.fields['avgTempOut'],
                   self.releve.fields['loTempOut'],
                   self.releve.fields['hiTempOut']))
        self.assertTrue(test)

    def test_fields_type(self):
        """
        Vérification des types de données météorologiques présentes dans le
        relevé
        """
        if not self.__connected:
            self.fail('Station non connectee')
        test_type = type(self.releve.fields['arcDateStation']) == str \
            and type(self.releve.fields['TempIn']) == float \
            and type(self.releve.fields['Pressure']) == float \
            and type(self.releve.fields['NextRec']) == int
        self.assertTrue(test_type)

    def test_to_new_file(self):
        """
        Test d'écriture du relevé dans un fichier
        """
        fichier = os.path.join(cfg.dirs['out'],
                               '%s_%s_%s.csv' %
                               (cfg.station['id'], self.releve.model,
                                self.releve.rtc['date'][:10]))
        if os.path.exists(fichier):
            os.remove(fichier)
        self.releve.to_file()
        self.assertTrue(os.path.exists(fichier))

    def test_to_web(self):
        """
        Test d'envoi du relevé sur le web
        """
        net = Internet(cfg.web['connection'], cfg.web['url'],
                       PROXIES, cfg.web['ssid'])
        net.request(cfg.web['url'], fields=self.releve.rtc)
        self.assertEqual(type(net.date), datetime)

    def test_fake_to_web(self):
        """
        Test d'envoi d'un relevé bidon
        """
        releve = Releve(cfg.station['id'], cfg.station['model'],
                        cfg.dirs['out'])
        date_st = datetime(1998, 7, 14, 6, 6, 6).strftime("%Y-%m-%d %H:%M:%S")
        dct_values = {'RainRate': 12, 'HumOut': 85.7, 'TempOut':50,
                      'WindDir': 92.5, 'WindSpeed': 0.1, 'Pressure': 1005.2,
                      'arcDateStation': date_st}
        releve.fields = dct_values
        dct_values['id'] = cfg.station['id']
        net = Internet(cfg.web['connection'], cfg.web['url'],
                       PROXIES, cfg.web['ssid'])
        net.request(cfg.web['url'], fields=releve.rtc)
        self.assertEqual(type(net.date), datetime)

class TestInternet(unittest.TestCase):
    """
    Tests unitaires de connexion au net
    """
    def test_internet(self):
        """
        Test de connexion avec internet
        """
        net = Internet(cfg.web['connection'], cfg.web['url'],
                       PROXIES, cfg.web['ssid'])
        self.assertTrue(net.connected)

    def test_internet_reconnect(self):
        """
        Test de reconnexion à internet (après déconnexion)
        NB : possible que pour une configuration Wifi, puisqu'on se connecte
        au PC via SSH ^^
        """
        if cfg.web['connection'] == 'wifi':
            os.system('ifdown wlan0')
            net = Internet(cfg.web['connection'], cfg.web['url'], PROXIES,
                           cfg.web['ssid'])
            connected = net.connected
        else:
            connected = True
        self.assertTrue(connected)


class TestConfig(unittest.TestCase):
    """
    Tests unitaires de lecture du fichier de configuration
    """
    def test_config_missing_file(self):
        """
        Test de configuration de l'appli à partir d'un fichier inexistant
        """
        self.assertRaises(ConfigParser.Error, config, '/path/inexistant')

    def test_config_read(self):
        """
        Test de lecture de la configuration de l'appli
        """
        configuration = config(CFG_FILE)
        self.assertEqual(configuration.read(), 0)

    def test_config_check(self):
        """
        Test de vérification de la configuration de l'appli
        """
        self.assertEqual(cfg.check(), 0)


class TestHttp(unittest.TestCase):
    """
    Tests sur les requêtes HTTP
    """
    def setUp(self):
        self.__http = HTTP_Handler(PROXIES)
        self.__url_ex = 'http://www.google.fr'
        self.__to_url = cfg.web['url']

    def test_get_wrong_url(self):
        """
        Test de requête GET sur le serveur, avec une URL fausse
        """
        wrong_url = 'http://osur.univ-rennes1.fr/meteo_rennes_faux/indewa.php'
        self.assertIsNone(self.__http.get_request(wrong_url))

    def test_get_request(self):
        """ Emission d'une requete GET et recuperation date
        """
        str_date = self.__http.get_request(self.__url_ex)
        if str_date is None:
            req_date = None
        else:
            req_date = datetime(*eut.parsedate(str_date)[:6])
        self.assertEqual(type(req_date), datetime)

    def test_get_url(self):
        """
        Test de requête GET sur un serveur toujours en fonctionnement
        """
        str_date = self.__http.get_request(self.__url_ex)
        try:
            req_date = datetime(*eut.parsedate(str_date)[:6])
        except TypeError:
            print("ERROR : %s received, instead of email date" % str_date)
            req_date = None
        self.assertEqual(type(req_date), datetime)

    def test_get_https_url(self):
        """
        Test de requête GET sur un site en https
        """
        url = 'https://accounts.google.com'
        str_date = self.__http.get_request(url)
        if str_date is None:
            req_date = None
        else:
            req_date = datetime(*eut.parsedate(str_date)[:6])
        self.assertEqual(type(req_date), datetime)

    def test_get_server_url(self):
        """
        Test de requête GET sur le serveur
        """
        str_date = self.__http.get_request(self.__to_url)
        try:
            req_date = datetime(*eut.parsedate(str_date)[:6])
        except TypeError:
            print("ERROR : %s received, instead of email date" % str_date)
            req_date = None
        self.assertEqual(type(req_date), datetime)

    def test_check_wrong_url(self):
        """
        Test de vérification de la communication avec le serveur, avec une URL
        fausse
        """
        wrong_url = 'http://osur.univ-rennes1.fr/meteo_rennes_faux/indewa.php'
        self.assertTrue(self.__http.check_url(wrong_url))

    def test_check_https_url(self):
        """
        Test de vérification de communication avec 1 serveur (https)
        """
        self.assertTrue(self.__http.check_url(self.__url_ex))

    def test_check_server_url(self):
        """
        Test de vérification de la communication avec le serveur
        """
        self.assertTrue(self.__http.check_url(self.__to_url))


if __name__ == '__main__':
    unittest.main()
