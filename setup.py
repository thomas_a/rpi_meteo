#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 23 14:27:17 2015

@author: thomas_a
"""
import os
import sys
import ConfigParser
import logging
import logging.config
import shutil


def install_rpiweather():
    """
    Installation de l'application rpi_weather :
    - logiciels
    - packages python
    - edition de PYTHONPATH
    - creation scripts au demarrage
    """

    # installation de logiciels
    log.info('1. INSTALLATION LOGICIELS')

    #configuration proxy
    if HTTP_PROXY == '':
        proxy = ''
    else:
        log.info('Ajout configuration proxy')
        proxy = '--proxy=%s' % HTTP_PROXY
        with open(os.path.expanduser('~/.bashrc'), 'a') as ofi:
            ofi.write('\nexport http_proxy=%s\n' % HTTP_PROXY)
        os.system('\nexport http_proxy=%s\n' % HTTP_PROXY)
        with open('/etc/apt/apt.conf', 'a') as ofi:
            ofi.write('\nAcquire::http::Proxy "%s";\n' % HTTP_PROXY)

    log.info('2. INSTALLATION DES PACKAGES...')
    lst_packages = ['python-pip', 'ntpdate', 'libyaml-dev', 'python-dev', 'python-serial']
    total_rep = 0
    for p in lst_packages:
        rep = os.system('apt-get install %s' % p)
        total_rep += rep
        rep = 'OK' if rep == 0 else 'Pb.'
        log.info('Installation de %s => %s', p, rep)
    log.info('Installation logiciels : %i/%i réussi', total_rep,
             len(lst_packages))

    log.info('3. INSTALLATION LIBRAIRIES PYTHON...')
    f_package = 'requirements.txt'
    if os.path.exists(f_package):
        rep = os.system('pip install %s -r %s' % (proxy, f_package))
        rep = 'OK' if rep == 0 else 'Pb.'
        log.info('Installation packages Python : %s', rep)
    else:
        log.error('Missing requirements file : %s', f_package)

    rep = os.system('pip install http://github.com/cmcginty/PyWeather/raw/master/dist/weather-0.9.1.tar.gz')
    rep = 'OK' if rep == 0 else 'Pb.'
    log.info('Installation pyweather : %s', rep)

    log.info('3. EDITION DU PYTHONPATH')
    try:
        path_python = os.environ['PYTHONPATH'].split(':')
    except KeyError:
        path_python = []

    if not DIR_BIN in path_python:
        # ajouter DIR_BIN à path
        sys.path.append(DIR_BIN)
        ofi = open(os.path.expanduser('~/.bashrc'), 'a')
        ofi.write('\nexport PYTHONPATH=$PYTHONPATH:%s\n' % DIR_BIN)
        ofi.close()
        log.info('=> Pythonpath mis à jour')
    else:
        log.info('=> Pythonpath à jour')

    log.info('4. CREATION SCRIPT AU DEMARRAGE')
    lst_script = ['daemon_weather', 'watchdog_weather']

    # installation daemon weather
    with open('/etc/init.d/%s' % lst_script[0], 'w') as ofi:
        ofi.write('### BEGIN INIT INFO\n')
        ofi.write('# Provides:          daemon_weather\n')
        ofi.write('# Required-Start:    $remote_fs $syslog $all\n')
        ofi.write('# Required-Stop:     $remote_fs $syslog\n')
        ofi.write('# Default-Start:     2 3 4 5\n')
        ofi.write('# Default-Stop:      0 1 6\n')
        ofi.write('# Short-Description: daemon pour rpi_weather\n')
        ofi.write('# Description:       recolte les donnees meteorologiques\n')
        ofi.write('### END INIT INFO\n')
        ofi.write('#!/bin/sh\n')
        ofi.write('export PYTHONPATH=$PYTHONPATH:%s\n' % DIR_BIN)
        ofi.write('python %s' % os.path.join(os.getcwd(), '%s.py $1' \
                  % lst_script[0]))

    # installation watchdog weather
    with open(lst_script[1], 'r') as ofi:
        bash_script = ofi.readlines()
    with open('/etc/init.d/%s' % lst_script[1], 'w') as ofi:
        for l in bash_script:
            if l.strip() == 'SCRIPT=':
                ofi.write("SCRIPT=%s\n" % os.path.join(os.getcwd(), \
                          '%s.py' % lst_script[1]))
            elif l.strip() == 'export PYTHONPATH=':
                ofi.write('export PYTHONPATH=$PYTHONPATH:%s\n' % DIR_BIN)
            elif l.strip() == 'fichier=':
                ofi.write('fichier=%s\n' %
                          os.path.join(DIR_OUT, WATCH_LOG))
            else:
                ofi.write(l)

    # transformation des scripts en services
    for script in lst_script:
        os.system('chmod 755 /etc/init.d/%s' % script)
        os.system('update-rc.d %s defaults' % script)


def remove_rpiweather():
    """
    Installation de l'application rpi_weather :
    - Suppression des scripts au demarrage
    """
    log.info('DESINSTALLATION de RPI WEATHER')

    log.info('1. SUPPRESSION SCRIPT AU DEMARRAGE...')
    lst_scripts = ['watchdog_weather', 'daemon_weather']
    for script in lst_scripts:
        os.system('update-rc.d -f %s remove' % script)
        if os.path.exists('/etc/init.d/%s' % script):
            try:
                os.unlink('/etc/init.d/%s' % script)
                os.remove('/etc/init.d/%s' % script)
            except (IOError, OSError):
                continue
        log.debug('%s supprime', script)
    log.info('=> Scripts supprimés')

    log.info('2. EDITION DE PYTHONPATH')
    motif = 'export PYTHONPATH=$PYTHONPATH:%s' % DIR_BIN
    with open(os.path.expanduser('~/.bashrc'), 'r') as ofi:
        bashrc = ofi.readlines()
    with open(os.path.expanduser('~/.bashrc'), 'w') as ofi:
        for l in bashrc:
            if l.strip() != motif:
                ofi.write(l)
    log.info('PYTHONPATH edite')

if __name__ == '__main__':

    pathname = os.path.dirname(sys.argv[0])
    os.chdir(pathname)

    # lecture config intiale du Raspberry
    cfgparser = ConfigParser.ConfigParser()
    cfgparser.read('rpi_meteo.cfg')
    DIR_OUT = cfgparser.get('os', 'dirOut')
    if not os.path.exists(DIR_OUT):
        os.makedirs(DIR_OUT)
    DIR_BIN = cfgparser.get('os', 'dirBin')
    WATCH_LOG = cfgparser.get('os', 'watchlog')
    HTTP_PROXY = cfgparser.get('web', 'http_proxy')
    cfgparser = None

    # creation du log
    if not os.path.exists(os.path.join(DIR_BIN, 'logging.cfg')):
        shutil.copyfile(os.path.join(DIR_BIN, 'loggingORIGINAL.cfg'),
                        os.path.join(DIR_BIN, 'logging.cfg'))
    logging.config.fileConfig('logging.cfg')
    log = logging.getLogger('rpi_log.setup')

    if sys.argv[1] == 'install':
        install_rpiweather()
    elif sys.argv[1] == 'remove':
        remove_rpiweather()
    else:
        log.warning('USAGE: python setup.py install|remove')
